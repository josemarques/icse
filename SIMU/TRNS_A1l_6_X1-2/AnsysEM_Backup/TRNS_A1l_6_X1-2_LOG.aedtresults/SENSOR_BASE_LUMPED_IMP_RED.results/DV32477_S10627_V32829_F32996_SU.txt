    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 2624281320.330081
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 136701
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 363.000000
    MemoryTotalMB 1069.000000
    NumMeshStatisticsTets 136701
    MemoryForSMatrixOnlyMB 633.011719
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 175360
    TotalModes 2
    NnzGrowthFactor 24.704510
