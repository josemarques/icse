    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 2616336927.981995
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 115233
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 321.000000
    MemoryTotalMB 844.000000
    NumMeshStatisticsTets 115233
    MemoryForSMatrixOnlyMB 573.859375
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 149827
    TotalModes 2
    NnzGrowthFactor 21.431420
