    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 2527399287.321826
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 131635
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 354.000000
    MemoryTotalMB 1083.000000
    NumMeshStatisticsTets 131635
    MemoryForSMatrixOnlyMB 848.214844
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 4
    NumKrylovVectors -1
    MatrixSize 169196
    TotalModes 2
    NnzGrowthFactor 24.412378
