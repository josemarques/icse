    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 3000000000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 137767
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 362.000000
    MemoryTotalMB 1090.000000
    NumMeshStatisticsTets 137767
    MemoryForSMatrixOnlyMB 735.808594
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 176476
    TotalModes 2
    NnzGrowthFactor 24.979956
