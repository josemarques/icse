    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 2139062500.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 115673
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 323.000000
    MemoryTotalMB 851.000000
    NumMeshStatisticsTets 115673
    MemoryForSMatrixOnlyMB 632.480469
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 150383
    TotalModes 2
    NnzGrowthFactor 21.656300
