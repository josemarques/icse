$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 18:39:19')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:14:47')
			I(1, 'ComEngine Memory', '174 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 123 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 174 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 18:39:20')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:15')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 132116, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 178092, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 195820, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 3, 0, 3, 0, 118972, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 18:39:35')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:34')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:39:35')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 185796, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 212636, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 416748, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 1, 0, 416748, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178416, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:39:44')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 140412, 'I(1, 2, \'Tetrahedra\', 90331, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208716, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64240, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 243848, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64240, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 5, 0, 544616, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88787, false, 3, \'Matrix bandwidth\', 7.804, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 544616, 'I(2, 1, \'Disk\', \'3.3 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178416, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.150906, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:40:02')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:20')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 155408, 'I(1, 2, \'Tetrahedra\', 109605, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 251888, 'I(2, 1, \'Disk\', \'43.5 KB\', 2, \'Tetrahedra\', 82752, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 2, 0, 281064, 'I(3, 1, \'Disk\', \'160 Bytes\', 2, \'Tetrahedra\', 82752, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 7, 0, 704116, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110965, false, 3, \'Matrix bandwidth\', 7.95532, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 704116, 'I(2, 1, \'Disk\', \'4.05 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178416, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0628295, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:40:22')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:24')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 173396, 'I(1, 2, \'Tetrahedra\', 134434, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286432, 'I(2, 1, \'Disk\', \'54.5 KB\', 2, \'Tetrahedra\', 106741, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 330248, 'I(3, 1, \'Disk\', \'605 Bytes\', 2, \'Tetrahedra\', 106741, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 5, 0, 10, 0, 914360, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 139593, false, 3, \'Matrix bandwidth\', 8.08296, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 914360, 'I(2, 1, \'Disk\', \'5.03 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178416, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0237505, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:40:46')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 171616, 'I(1, 2, \'Tetrahedra\', 142772, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 298732, 'I(2, 1, \'Disk\', \'58.2 KB\', 2, \'Tetrahedra\', 114644, false)', true, true)
				ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346716, 'I(3, 1, \'Disk\', \'100 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 6, 0, 11, 0, 1019164, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 2, 0, 3, 0, 1019164, 'I(2, 1, \'Disk\', \'3.5 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178416, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.00623973, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 18:41:10')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:12:57')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 18:41:10')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:12:57')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282000, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 346808, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 563112, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 563112, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282360, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 345996, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 587552, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 587552, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282740, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 371624, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 732732, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 732732, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283796, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 370764, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 689036, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 689036, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283152, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 371232, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1189508, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1189508, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283716, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346292, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 598772, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 598772, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283964, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346864, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 618644, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 618644, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 160.192%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 145.609%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283612, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346496, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 645656, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 645656, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 550MHz; S Matrix Error =   4.750%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346664, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 646168, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 646168, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.275GHz; S Matrix Error =  14.391%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283176, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347004, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 612068, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 612068, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.6375GHz; S Matrix Error =   1.163%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282980, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346288, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 641116, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 641116, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 2.81875GHz; S Matrix Error =   0.477%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283312, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 346620, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 614032, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 614032, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.884%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282800, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346012, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 574992, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 574992, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.636%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283484, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 346460, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 613544, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 613544, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.033%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283136, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346408, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 660064, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 660064, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.033%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282968, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347028, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 597160, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 597160, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.116%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284008, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346240, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 604308, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 604308, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.082421875GHz; S Matrix Error =   0.144%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282568, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346544, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 617056, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 617056, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0880859375GHz; S Matrix Error =   0.086%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283576, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346860, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 588076, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 588076, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.047%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282736, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346652, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 597476, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 597476, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; S Matrix Error =   0.012%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283252, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347076, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 606880, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 606880, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 325MHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282040, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347064, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 633704, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 633704, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 212.5MHz; S Matrix Error =   0.004%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283516, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 370492, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 694796, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 694796, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 32.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283868, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346528, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 581836, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 581836, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 77.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283800, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346436, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 632936, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 632936, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 88.75MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281732, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346428, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 621396, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 621396, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.45625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283776, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 346476, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 587700, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 587700, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 2.45625GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60778269567392GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281748, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346364, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 9, 0, 647676, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 647676, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 2.60778269567392GHz; Scattering matrix quantities converged; Passivity Error =   0.001680; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178420, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61678194548637GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282868, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 346936, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 623880, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 623880, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 2.61678194548637GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61228232058015GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283624, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 345888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 612676, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 612676, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 2.61228232058015GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61453213303326GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283936, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346520, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 580348, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 580348, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 2.61453213303326GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.53201634783696GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283788, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 348536, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 586168, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 586168, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.53201634783696GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282484, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 371320, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 708056, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 708056, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283260, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 6, 0, 371620, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1180268, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1180268, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283804, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346432, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 634748, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 634748, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283844, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 346708, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 597764, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 597764, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 371884, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 712756, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 712756, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282876, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 371372, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 730992, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 730992, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283164, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346712, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 642684, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 642684, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282964, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346372, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 650456, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 650456, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178424, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283540, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346596, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 650756, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 650756, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283128, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346064, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 593872, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 593872, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282396, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 371956, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1205628, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1205628, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.97734375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281692, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 345772, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 591976, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 591976, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 2.97734375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282900, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346768, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 610056, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 610056, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283796, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346108, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 624192, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 624192, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.28125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:17')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283248, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372060, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1193096, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 12.673, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1193096, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 1.28125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.56989952175544GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283836, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 346624, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 620664, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 620664, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 2.56989952175544GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.58884110871468GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283284, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346680, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114644, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 623180, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149145, false, 3, \'Matrix bandwidth\', 8.11003, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 623180, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 2.58884110871468GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178428, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'174 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:15\', 1, \'Total Memory\', \'191 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:34\', 1, \'Average memory/process\', \'995 MB\', 1, \'Max memory/process\', \'995 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:12:57\', 1, \'Average memory/process\', \'1.15 GB\', 1, \'Max memory/process\', \'1.15 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 114644, false, 2, \'Max matrix size\', 149145, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 18:54:07\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
