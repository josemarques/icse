    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 88750000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 135868
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 359.000000
    MemoryTotalMB 1057.000000
    NumMeshStatisticsTets 135868
    MemoryForSMatrixOnlyMB 705.753906
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 174358
    TotalModes 2
    NnzGrowthFactor 24.341062
