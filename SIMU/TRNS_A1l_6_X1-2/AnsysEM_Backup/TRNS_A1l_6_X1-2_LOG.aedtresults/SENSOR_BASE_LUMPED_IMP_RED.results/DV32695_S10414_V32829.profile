$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 20:04:31')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:14:50')
			I(1, 'ComEngine Memory', '175 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 123 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 175 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 20:04:32')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:16')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 132148, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 176228, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 195780, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 3, 0, 3, 0, 118460, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 20:04:48')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:36')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 20:04:48')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 185536, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 212196, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 414796, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 1, 0, 414796, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179116, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 20:04:57')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 140524, 'I(1, 2, \'Tetrahedra\', 90328, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208736, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64185, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 2, 0, 243596, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64185, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 5, 0, 551824, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88774, false, 3, \'Matrix bandwidth\', 7.80053, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 551824, 'I(2, 1, \'Disk\', \'3.29 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179116, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.139743, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 20:05:15')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:20')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 155988, 'I(1, 2, \'Tetrahedra\', 109586, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 251824, 'I(2, 1, \'Disk\', \'43.1 KB\', 2, \'Tetrahedra\', 82708, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 2, 0, 280772, 'I(3, 1, \'Disk\', \'165 Bytes\', 2, \'Tetrahedra\', 82708, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 7, 0, 713928, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110961, false, 3, \'Matrix bandwidth\', 7.95292, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 713928, 'I(2, 1, \'Disk\', \'4.05 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179116, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.054739, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 20:05:36')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:24')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 173744, 'I(1, 2, \'Tetrahedra\', 134403, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286368, 'I(2, 1, \'Disk\', \'53.4 KB\', 2, \'Tetrahedra\', 106656, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 329812, 'I(3, 1, \'Disk\', \'300 Bytes\', 2, \'Tetrahedra\', 106656, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 5, 0, 10, 0, 937208, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 139559, false, 3, \'Matrix bandwidth\', 8.08105, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 937208, 'I(2, 1, \'Disk\', \'5.03 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179116, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0234194, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 20:06:00')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:24')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 173104, 'I(1, 2, \'Tetrahedra\', 143818, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 303344, 'I(2, 1, \'Disk\', \'57.1 KB\', 2, \'Tetrahedra\', 115673, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349744, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 6, 0, 11, 0, 1019768, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 2, 0, 3, 0, 1019768, 'I(2, 1, \'Disk\', \'3.63 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179116, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.00684149, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 20:06:25')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:12:56')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 20:06:25')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:12:56')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284704, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349084, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 606884, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 606884, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286292, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 351300, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 640704, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 640704, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285860, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 374800, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 706136, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 706136, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:14')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286272, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 373564, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 797156, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 797156, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286064, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 375336, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1249824, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1249824, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286708, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349492, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 625728, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 625728, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286936, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349444, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 615296, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 615296, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 146.458%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179120, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 131.413%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286036, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 351244, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 636604, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 636604, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 550MHz; S Matrix Error =  56.217%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284900, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 350732, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 608064, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 608064, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.275GHz; S Matrix Error =  47.497%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286048, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349348, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 634792, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 634792, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 1.9125GHz; S Matrix Error =   3.979%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285660, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350236, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 615676, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 615676, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 2.6375GHz; S Matrix Error =   3.656%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 348612, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 9, 0, 579720, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 579720, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 2.81875GHz; S Matrix Error =   0.739%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286688, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 348524, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 615816, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 615816, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.652%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286460, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 348996, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 566708, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 566708, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.184375GHz; S Matrix Error =   0.055%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286396, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 350008, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 561592, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 561592, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.003125GHz; S Matrix Error =   0.057%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285644, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 351724, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 619188, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 619188, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.0484375GHz; S Matrix Error =   0.057%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286004, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349576, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 602380, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 602380, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.07109375GHz; S Matrix Error =   0.097%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286980, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349656, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 659880, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 659880, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.082421875GHz; S Matrix Error =   0.220%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285072, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 350008, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 616848, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 616848, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.0880859375GHz; S Matrix Error =   0.083%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286352, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349624, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 647660, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 647660, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; Scattering matrix quantities converged; Passivity Error =   0.000645; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 192.234058514629MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286096, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349564, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 570588, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 570588, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 192.234058514629MHz; Scattering matrix quantities converged; Passivity Error =   0.000787; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.10607451862966GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285236, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350228, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 613592, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 613592, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.10607451862966GHz; Scattering matrix quantities converged; Passivity Error =   0.000187; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.57478544636159GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286264, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349372, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 590856, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 590856, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.57478544636159GHz; Scattering matrix quantities converged; Passivity Error =   0.003273; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61153238309577GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286840, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349932, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 640828, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 640828, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 2.61153238309577GHz; Scattering matrix quantities converged; Passivity Error =   0.001925; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.64377969492373GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286468, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349960, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 622148, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 622148, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.64377969492373GHz; Scattering matrix quantities converged; Passivity Error =   0.000367; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60628282070518GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285948, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 348992, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 562884, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 562884, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.60628282070518GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:14')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286008, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 373708, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 774772, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 774772, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 32.5MHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.62451619154788GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285172, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 350288, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 657360, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 657360, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 2.62451619154788GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285504, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349948, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 624348, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 624348, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 77.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284944, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349940, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 637380, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 637380, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 371.117029257314MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286956, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349720, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 630132, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 630132, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 371.117029257314MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 281.675543885972MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286552, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349772, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 627548, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 627548, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 281.675543885972MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:14')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286784, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 374804, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 771204, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 771204, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286768, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 375844, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1273680, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1273680, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 146.117029257314MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285492, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349992, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 647808, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 647808, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 146.117029257314MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 123.058514628657MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286488, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349024, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 617108, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 617108, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 123.058514628657MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286140, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 374088, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 728476, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 728476, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286980, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 373816, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 693528, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 693528, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286124, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349244, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 606996, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 606996, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285136, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 350384, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 615240, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 615240, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179124, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286568, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349552, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 594928, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 594928, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179128, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286788, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349792, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 587888, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 587888, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179128, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285564, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349800, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 614952, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 614952, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179128, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285760, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 350308, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 645356, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 645356, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179128, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.1375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286828, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 641432, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 641432, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 1.1375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179128, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286176, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 375108, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1266292, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1266292, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179128, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 460.558514628657MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285176, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 349772, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 623164, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 623164, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 460.558514628657MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179128, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.28125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286932, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 376052, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1210460, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 12.6848, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1210460, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 1.28125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179132, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 169.175543885972MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 285956, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 350016, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115673, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 585164, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 150383, false, 3, \'Matrix bandwidth\', 8.11489, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 585164, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 169.175543885972MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 179132, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'175 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:16\', 1, \'Total Memory\', \'191 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:36\', 1, \'Average memory/process\', \'996 MB\', 1, \'Max memory/process\', \'996 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:12:56\', 1, \'Average memory/process\', \'1.21 GB\', 1, \'Max memory/process\', \'1.21 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 115673, false, 2, \'Max matrix size\', 150383, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 20:19:22\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
