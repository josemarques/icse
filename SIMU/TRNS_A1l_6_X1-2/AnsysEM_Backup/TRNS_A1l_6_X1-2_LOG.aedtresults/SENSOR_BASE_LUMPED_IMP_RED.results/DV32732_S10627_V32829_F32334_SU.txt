    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 1562500.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 123909
    BasisOrder 0
    TREECOTREE 1
    MemoryMasterMB 353.000000
    MemoryTotalMB 1283.546875
    NumMeshStatisticsTets 123909
    MemoryForSMatrixOnlyMB 770.323891
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 1
    MemorySMatrixOnlyAccurate 0
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 160210
    TotalModes 2
    NnzGrowthFactor 17.897747
