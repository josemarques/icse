$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 19:08:57')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:15:02')
			I(1, 'ComEngine Memory', '175 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 123 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 174 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 19:08:57')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:15')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 132264, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 176324, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 195836, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 3, 0, 3, 0, 119384, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 19:09:13')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:39')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 19:09:13')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 185380, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 212872, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 436568, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 436568, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178708, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 19:09:22')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 140676, 'I(1, 2, \'Tetrahedra\', 90328, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208660, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64209, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 243628, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64209, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 5, 0, 564804, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88771, false, 3, \'Matrix bandwidth\', 7.80266, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 564804, 'I(2, 1, \'Disk\', \'3.3 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178708, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.154535, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 19:09:40')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:20')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 155236, 'I(1, 2, \'Tetrahedra\', 109595, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 251744, 'I(2, 1, \'Disk\', \'43.5 KB\', 2, \'Tetrahedra\', 82733, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 280920, 'I(3, 1, \'Disk\', \'430 Bytes\', 2, \'Tetrahedra\', 82733, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 7, 0, 719212, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110972, false, 3, \'Matrix bandwidth\', 7.9538, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 719212, 'I(2, 1, \'Disk\', \'4.05 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178708, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.053682, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 19:10:01')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:26')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 173648, 'I(1, 2, \'Tetrahedra\', 134425, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286328, 'I(2, 1, \'Disk\', \'53.8 KB\', 2, \'Tetrahedra\', 106725, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 330100, 'I(3, 1, \'Disk\', \'30 Bytes\', 2, \'Tetrahedra\', 106725, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 5, 0, 10, 0, 983696, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 139614, false, 3, \'Matrix bandwidth\', 8.08202, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 2, 0, 3, 0, 983696, 'I(2, 1, \'Disk\', \'5.03 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178708, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0243146, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 19:10:27')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:24')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 173888, 'I(1, 2, \'Tetrahedra\', 145162, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 303528, 'I(2, 1, \'Disk\', \'56.7 KB\', 2, \'Tetrahedra\', 116929, false)', true, true)
				ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352308, 'I(3, 1, \'Disk\', \'520 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 6, 0, 11, 0, 1027824, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 2, 0, 3, 0, 1027824, 'I(2, 1, \'Disk\', \'3.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178708, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.00606928, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 19:10:52')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:13:07')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 19:10:52')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:13:07')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288612, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352828, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 621232, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 621232, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288856, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352720, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 635368, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 635368, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288044, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 377504, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 811068, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 811068, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286924, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 376984, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 779276, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 779276, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287748, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 379136, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1279936, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1279936, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288236, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352456, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 648368, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 648368, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 288996, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 352972, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 615476, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 615476, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 161.573%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178712, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 140.309%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 288180, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352572, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 614952, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 614952, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 550MHz; S Matrix Error =  10.762%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178712, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287920, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352484, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 614804, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 614804, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.275GHz; S Matrix Error =  22.341%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287496, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 352452, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 560604, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 560604, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.6375GHz; S Matrix Error =   1.006%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288004, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352472, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 609984, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 609984, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 2.81875GHz; S Matrix Error =   0.419%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287884, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352916, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 592860, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 592860, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.857%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 288780, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 351960, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 685320, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 685320, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.637%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287096, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352240, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 570572, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 570572, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.031%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 289016, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352652, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 577352, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 577352, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.038%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288992, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 351328, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 587608, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 587608, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.123%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288008, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 351608, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 643320, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 643320, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.082421875GHz; S Matrix Error =   0.153%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287560, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352548, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 638996, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 638996, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0880859375GHz; S Matrix Error =   0.097%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288068, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352144, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 630788, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 630788, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.067%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287688, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352088, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 635020, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 635020, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; S Matrix Error =   0.010%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288464, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352656, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 632372, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 632372, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 325MHz; Scattering matrix quantities converged; Passivity Error =   0.000268; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.10607451862966GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288352, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 352348, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 607468, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 607468, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.10607451862966GHz; Scattering matrix quantities converged; Passivity Error =   0.000560; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59803350837709GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 288200, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352524, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 640080, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 640080, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.59803350837709GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 289064, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 377564, 'I(3, 1, \'Disk\', \'12 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 801900, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 801900, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 32.5MHz; S Matrix Error =   0.003%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61776675418855GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288056, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 353712, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 654760, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 654760, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.61776675418855GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59053413353338GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288880, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352880, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 598904, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 598904, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.59053413353338GHz; Scattering matrix quantities converged; Passivity Error =   0.000606; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61228232058015GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288492, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352668, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 577960, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 577960, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 2.61228232058015GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287084, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 353692, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 605104, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 605104, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 77.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 287940, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 351224, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 611192, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 611192, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 88.75MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60515791447862GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 288876, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 353524, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 587784, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 587784, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 2.60515791447862GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288012, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352948, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 620728, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 620728, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 212.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287908, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 352200, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 629788, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 629788, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288488, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 378064, 'I(3, 1, \'Disk\', \'19 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 760176, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 760176, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:17')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288580, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 378584, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 16, 0, 1224992, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1224992, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287324, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352980, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 651664, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 651664, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288208, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 376852, 'I(3, 1, \'Disk\', \'3 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 723508, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 723508, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286948, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 377800, 'I(3, 1, \'Disk\', \'13 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 800236, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 800236, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287784, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352980, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 589188, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 589188, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287596, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352588, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 623176, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 623176, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 288836, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 352332, 'I(3, 1, \'Disk\', \'9 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 601812, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 601812, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178716, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288136, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352484, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 605004, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 605004, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287780, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352604, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 680748, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 680748, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 287568, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 378744, 'I(3, 1, \'Disk\', \'26 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1260812, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1260812, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.97734375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288468, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 352596, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 616684, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 616684, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 2.97734375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288116, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 352392, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 602388, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 602388, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 288996, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 353108, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 624820, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 8.11736, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 624820, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.28125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 288704, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 379332, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1203828, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1203828, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 1.28125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 12.8125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286860, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 377672, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 727868, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 727868, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 12.8125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 14.21875MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 288836, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 116929, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 377384, 'I(3, 1, \'Disk\', \'9 Bytes\', 2, \'Tetrahedra\', 116929, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 727796, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 151907, false, 3, \'Matrix bandwidth\', 12.6921, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 727796, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 14.21875MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178720, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'174 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:15\', 1, \'Total Memory\', \'191 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:39\', 1, \'Average memory/process\', \'1e+03 MB\', 1, \'Max memory/process\', \'1e+03 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:13:07\', 1, \'Average memory/process\', \'1.22 GB\', 1, \'Max memory/process\', \'1.22 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 116929, false, 2, \'Max matrix size\', 151907, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 19:24:00\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
