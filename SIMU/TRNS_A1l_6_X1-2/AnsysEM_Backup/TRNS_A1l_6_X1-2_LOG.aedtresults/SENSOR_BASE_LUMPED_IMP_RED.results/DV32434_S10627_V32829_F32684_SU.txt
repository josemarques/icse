    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 2594090671.105278
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 114671
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 320.000000
    MemoryTotalMB 850.000000
    NumMeshStatisticsTets 114671
    MemoryForSMatrixOnlyMB 581.039062
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 149134
    TotalModes 2
    NnzGrowthFactor 21.894109
