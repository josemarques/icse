    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 88750000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 100027
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 293.000000
    MemoryTotalMB 724.000000
    NumMeshStatisticsTets 100027
    MemoryForSMatrixOnlyMB 498.957031
    LowFrequencyCutoff 2513676.340460
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 131494
    TotalModes 2
    NnzGrowthFactor 19.286472
