    VersionID HFSS2019
    NumberFrequencies 1
    Frequency 1368750000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 100149
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 270.000000
    MemoryTotalMB 681.000000
    NumMeshStatisticsTets 100149
    MemoryForSMatrixOnlyMB 387.230469
    LowFrequencyCutoff 3414070.806083
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 131534
    TotalModes 2
