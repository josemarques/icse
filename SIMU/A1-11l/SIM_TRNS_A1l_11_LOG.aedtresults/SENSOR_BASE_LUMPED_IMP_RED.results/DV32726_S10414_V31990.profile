$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 12:26:33')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:11:41')
			I(1, 'ComEngine Memory', '172 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 124 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 172 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 12:26:33')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:14')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 7, 0, 7, 0, 128116, 'I(1, 2, \'Tetrahedra\', 71652, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 172128, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 188096, 'I(2, 1, \'Disk\', \'4.81 KB\', 2, \'Tetrahedra\', 46851, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 116044, 'I(1, 2, \'Tetrahedra\', 71873, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 12:26:48')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:05')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 12:26:48')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 1, 0, 1, 0, 180004, 'I(2, 1, \'Disk\', \'23.6 KB\', 2, \'Tetrahedra\', 47036, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 205072, 'I(3, 1, \'Disk\', \'64.6 KB\', 2, \'Tetrahedra\', 47036, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 442680, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 67691, false, 3, \'Matrix bandwidth\', 7.60611, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 442680, 'I(2, 1, \'Disk\', \'6.41 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176408, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 12:26:57')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:14')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 137156, 'I(1, 2, \'Tetrahedra\', 85984, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 202128, 'I(2, 1, \'Disk\', \'31.7 KB\', 2, \'Tetrahedra\', 60405, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 234652, 'I(3, 1, \'Disk\', \'214 Bytes\', 2, \'Tetrahedra\', 60405, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 4, 0, 525868, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 83793, false, 3, \'Matrix bandwidth\', 7.78641, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 525868, 'I(2, 1, \'Disk\', \'3.13 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176408, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.152027, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 12:27:12')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 151008, 'I(1, 2, \'Tetrahedra\', 104110, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 228396, 'I(2, 1, \'Disk\', \'38.6 KB\', 2, \'Tetrahedra\', 77684, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 270656, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 77684, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 6, 0, 682536, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 104610, false, 3, \'Matrix bandwidth\', 7.93603, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 682536, 'I(2, 1, \'Disk\', \'3.82 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176408, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0715818, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 12:27:29')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 7, 0, 7, 0, 168424, 'I(1, 2, \'Tetrahedra\', 127416, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 277500, 'I(2, 1, \'Disk\', \'48 KB\', 2, \'Tetrahedra\', 99963, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315604, 'I(3, 1, \'Disk\', \'300 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 4, 0, 8, 0, 861396, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 861396, 'I(2, 1, \'Disk\', \'4.7 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176408, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0112507, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 12:27:53')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:10:21')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 12:27:53')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:10:21')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259908, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316436, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 520036, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 520036, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258244, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315340, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 554832, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 554832, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259976, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315732, 'I(3, 1, \'Disk\', \'12 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 577100, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 577100, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258544, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 315080, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 571584, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 571584, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260108, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338508, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 1021696, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 12.5955, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1021696, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259228, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315948, 'I(3, 1, \'Disk\', \'19 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 499452, 'I(4, 1, \'Disk\', \'6 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 499452, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259924, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315908, 'I(3, 1, \'Disk\', \'15 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 560888, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 560888, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 118.478%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176408, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 126.591%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259104, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316296, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 511124, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 511124, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 2.275GHz; S Matrix Error =  52.306%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259588, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315900, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 537220, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 537220, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 1.9125GHz; S Matrix Error =   1.360%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259988, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315828, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 499812, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 499812, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 550MHz; S Matrix Error =   0.373%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260064, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316028, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 501004, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 501004, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 325MHz; S Matrix Error =   0.036%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260260, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315420, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 512952, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 512952, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 2.6375GHz; S Matrix Error =   0.046%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258404, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316228, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 520260, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 520260, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.81875GHz; S Matrix Error =   0.039%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.45625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 260112, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315960, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 550416, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 550416, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.45625GHz; S Matrix Error =   0.052%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.73125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259208, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315196, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 537348, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 537348, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 1.73125GHz; S Matrix Error =   0.070%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258784, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315676, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 547116, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 547116, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.09375GHz; S Matrix Error =   0.086%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258628, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315644, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 503960, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 503960, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.184375GHz; S Matrix Error =   0.059%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258696, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315652, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 541004, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 541004, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.003125GHz; S Matrix Error =   0.057%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259388, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316100, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 561576, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 561576, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.0484375GHz; S Matrix Error =   0.071%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.02578125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259456, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315464, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 499904, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 499904, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.02578125GHz; S Matrix Error =   0.008%; Secondary solver criterion is not converged\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258716, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316060, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 577976, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 577976, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.546875GHz; S Matrix Error =   0.006%; Secondary solver criterion is not converged\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60253313328332GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259816, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315520, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 550068, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 550068, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.60253313328332GHz; Scattering matrix quantities converged; Passivity Error =   0.004381; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176412, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61453213303326GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259616, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315796, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 519388, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 519388, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.61453213303326GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259120, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315096, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 562956, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 562956, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 32.5MHz; S Matrix Error =   0.006%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60853263315829GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259616, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315372, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 549232, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 549232, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.60853263315829GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.5927839459865GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259652, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316196, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 513764, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 513764, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.5927839459865GHz; Scattering matrix quantities converged; Passivity Error =   0.011923; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.04382970742686GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260000, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 316144, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 519184, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 519184, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 2.04382970742686GHz; Scattering matrix quantities converged; Passivity Error =   0.003424; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09782520630158GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259496, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315868, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 500136, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 500136, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 2.09782520630158GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.14110010315079GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258112, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316032, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 532200, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 532200, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 2.14110010315079GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259644, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315632, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 522388, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 522388, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 77.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258292, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 524140, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 524140, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316328, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 507272, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507272, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 212.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259244, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315492, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 480480, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 480480, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258336, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316092, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 551172, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 551172, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 257892, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315316, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 496132, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 496132, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259556, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 337988, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 629340, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 12.5955, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 629340, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259392, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338152, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 948812, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 12.5955, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 948812, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 260164, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316408, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 492512, 'I(4, 1, \'Disk\', \'6 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 492512, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259552, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315800, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 555580, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 555580, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259876, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316132, 'I(3, 1, \'Disk\', \'15 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 566944, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 566944, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176416, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259752, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315424, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 487820, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 487820, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259812, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 339040, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 993464, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 12.5955, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 993464, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259484, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315616, 'I(3, 1, \'Disk\', \'13 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 548608, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 548608, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258620, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315616, 'I(3, 1, \'Disk\', \'6 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 538464, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 538464, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259656, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316020, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 563016, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 563016, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258400, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315824, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 538840, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 538840, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 268.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259264, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315952, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 544292, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 544292, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 268.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 7.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259348, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315532, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 511700, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 511700, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 7.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 114.0625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259296, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315880, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99963, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 507956, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05724, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507956, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 114.0625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176420, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'172 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:14\', 1, \'Total Memory\', \'184 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:05\', 1, \'Average memory/process\', \'841 MB\', 1, \'Max memory/process\', \'841 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:10:21\', 1, \'Average memory/process\', \'998 MB\', 1, \'Max memory/process\', \'998 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 99963, false, 2, \'Max matrix size\', 131445, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 12:38:14\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
