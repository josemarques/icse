    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 1210937.500000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 112810
    BasisOrder 0
    TREECOTREE 1
    MemoryMasterMB 331.000000
    MemoryTotalMB 1190.230469
    NumMeshStatisticsTets 112810
    MemoryForSMatrixOnlyMB 752.416275
    LowFrequencyCutoff 2727501.210306
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 1
    MemorySMatrixOnlyAccurate 0
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 146963
    TotalModes 2
    NnzGrowthFactor 16.250510
