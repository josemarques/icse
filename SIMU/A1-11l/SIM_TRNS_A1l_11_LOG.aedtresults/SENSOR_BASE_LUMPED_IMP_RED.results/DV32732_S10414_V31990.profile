$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 12:35:33')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:10:59')
			I(1, 'ComEngine Memory', '173 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 124 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 173 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 12:35:34')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:14')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 127668, 'I(1, 2, \'Tetrahedra\', 71652, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 173360, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 188916, 'I(2, 1, \'Disk\', \'4.81 KB\', 2, \'Tetrahedra\', 46851, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 115760, 'I(1, 2, \'Tetrahedra\', 71873, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 12:35:48')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:05')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 12:35:48')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 180476, 'I(2, 1, \'Disk\', \'23.6 KB\', 2, \'Tetrahedra\', 47036, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 205400, 'I(3, 1, \'Disk\', \'64.6 KB\', 2, \'Tetrahedra\', 47036, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 418820, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 67691, false, 3, \'Matrix bandwidth\', 7.60611, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 418820, 'I(2, 1, \'Disk\', \'6.41 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176768, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 12:35:57')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:14')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 137288, 'I(1, 2, \'Tetrahedra\', 85985, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 201848, 'I(2, 1, \'Disk\', \'31.4 KB\', 2, \'Tetrahedra\', 60391, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 234296, 'I(3, 1, \'Disk\', \'214 Bytes\', 2, \'Tetrahedra\', 60391, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 4, 0, 513592, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 83780, false, 3, \'Matrix bandwidth\', 7.78603, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 513592, 'I(2, 1, \'Disk\', \'3.13 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176768, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.159386, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 12:36:12')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 150560, 'I(1, 2, \'Tetrahedra\', 104110, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 228380, 'I(2, 1, \'Disk\', \'38.2 KB\', 2, \'Tetrahedra\', 77583, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 2, 0, 270400, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 77583, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 5, 0, 638024, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 104555, false, 3, \'Matrix bandwidth\', 7.93236, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 638024, 'I(2, 1, \'Disk\', \'3.81 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176768, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0715012, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 12:36:30')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 7, 0, 168256, 'I(1, 2, \'Tetrahedra\', 127386, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 277520, 'I(2, 1, \'Disk\', \'46.6 KB\', 2, \'Tetrahedra\', 99885, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315704, 'I(3, 1, \'Disk\', \'300 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 4, 0, 8, 0, 830156, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 830156, 'I(2, 1, \'Disk\', \'4.7 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176768, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0115319, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 12:36:54')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:09:38')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 12:36:54')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:09:38')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259052, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315948, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 507156, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507156, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258340, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316040, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 530856, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 530856, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259536, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315692, 'I(3, 1, \'Disk\', \'13 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 530616, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 530616, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259436, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315364, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 512136, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 512136, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259560, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338280, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 1014400, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 12.596, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1014400, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260056, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315756, 'I(3, 1, \'Disk\', \'19 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 511888, 'I(4, 1, \'Disk\', \'6 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 511888, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258424, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316180, 'I(3, 1, \'Disk\', \'15 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 548436, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 548436, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 118.720%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176772, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 127.747%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258124, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315356, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 547608, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 547608, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 2.275GHz; S Matrix Error =  52.718%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259752, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315724, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 544248, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 544248, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 1.9125GHz; S Matrix Error =   1.387%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258288, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315708, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 506920, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506920, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 550MHz; S Matrix Error =   0.372%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259560, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315788, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 552548, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 552548, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 325MHz; S Matrix Error =   0.034%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260108, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315952, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 502628, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 502628, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 2.6375GHz; S Matrix Error =   0.040%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258492, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316112, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 513460, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 513460, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.81875GHz; S Matrix Error =   0.033%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.45625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 259040, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316196, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 550396, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 550396, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.45625GHz; S Matrix Error =   0.084%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.73125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259936, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315616, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 514800, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 514800, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 1.73125GHz; S Matrix Error =   0.093%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259584, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316048, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 495356, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 495356, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.09375GHz; S Matrix Error =   0.082%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258692, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315756, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 505224, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 505224, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.003125GHz; S Matrix Error =   0.062%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259760, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316784, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 519420, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 519420, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0484375GHz; S Matrix Error =   0.065%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.02578125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258728, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316180, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 568592, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 568592, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.02578125GHz; S Matrix Error =   0.026%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.037109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 258824, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316016, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 536428, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 536428, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.037109375GHz; S Matrix Error =   0.005%; Secondary solver criterion is not converged\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258436, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315660, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 512580, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 512580, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.546875GHz; S Matrix Error =   0.004%; Secondary solver criterion is not converged\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176776, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.08507626906727GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 258700, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315596, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 507908, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507908, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.08507626906727GHz; Scattering matrix quantities converged; Passivity Error =   0.009061; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60178319579895GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258508, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315724, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 559304, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 559304, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.60178319579895GHz; Scattering matrix quantities converged; Passivity Error =   0.015931; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.08657614403601GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 2, 0, 258600, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315436, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 541692, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 541692, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 2.08657614403601GHz; Scattering matrix quantities converged; Passivity Error =   0.000850; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09932508127032GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258384, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315224, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 498732, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 498732, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.09932508127032GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258184, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315628, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 485800, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 485800, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 32.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259556, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315816, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 583520, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 583520, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 77.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258592, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315660, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 546720, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 546720, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 88.75MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 258636, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315592, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 500788, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 500788, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 212.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258892, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315936, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 506824, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506824, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.57432909789948GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258640, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315536, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 559444, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 559444, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 2.57432909789948GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61228232058015GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 259376, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316052, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 535792, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 535792, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.61228232058015GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.62489116029007GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258308, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315476, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 506460, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506460, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 2.62489116029007GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 258664, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315860, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 549988, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 549988, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259800, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316496, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 497260, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 497260, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258504, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316032, 'I(3, 1, \'Disk\', \'16 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 499708, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 499708, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 259468, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338912, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 9, 0, 632536, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 12.596, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 632536, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258460, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 317080, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 508508, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 508508, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258640, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316528, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 528380, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 528380, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259484, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338684, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 1060584, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 12.596, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1060584, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176780, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258384, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315704, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 530692, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 530692, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 258728, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338472, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 965888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 12.596, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 965888, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258452, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315952, 'I(3, 1, \'Disk\', \'19 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 494496, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 494496, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258316, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316388, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 499896, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 499896, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260084, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315564, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 511996, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 511996, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 114.0625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258420, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316044, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 519492, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 519492, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 114.0625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 258540, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316636, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 506044, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506044, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258292, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315876, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 534628, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 534628, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.4125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258236, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316076, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99885, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 518232, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131374, false, 3, \'Matrix bandwidth\', 8.05635, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 518232, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 1.4125GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176784, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'173 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:14\', 1, \'Total Memory\', \'185 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:05\', 1, \'Average memory/process\', \'811 MB\', 1, \'Max memory/process\', \'811 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:09:38\', 1, \'Average memory/process\', \'1.01 GB\', 1, \'Max memory/process\', \'1.01 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 99885, false, 2, \'Max matrix size\', 131374, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 12:46:32\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
