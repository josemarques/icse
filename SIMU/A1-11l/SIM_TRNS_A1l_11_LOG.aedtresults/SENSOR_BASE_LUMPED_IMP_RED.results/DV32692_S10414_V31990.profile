$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 11:48:40')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:11:40')
			I(1, 'ComEngine Memory', '172 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 124 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 172 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 11:48:41')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:14')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 7, 0, 7, 0, 127252, 'I(1, 2, \'Tetrahedra\', 71652, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 172816, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 189192, 'I(2, 1, \'Disk\', \'4.81 KB\', 2, \'Tetrahedra\', 46851, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 116056, 'I(1, 2, \'Tetrahedra\', 71873, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 11:48:55')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:03')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:48:55')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:07')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 1, 0, 1, 0, 180184, 'I(2, 1, \'Disk\', \'23.6 KB\', 2, \'Tetrahedra\', 47036, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 205404, 'I(3, 1, \'Disk\', \'64.6 KB\', 2, \'Tetrahedra\', 47036, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 427332, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 67691, false, 3, \'Matrix bandwidth\', 7.60611, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 1, 0, 427332, 'I(2, 1, \'Disk\', \'6.41 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176536, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:49:02')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:15')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 137528, 'I(1, 2, \'Tetrahedra\', 85991, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 202300, 'I(2, 1, \'Disk\', \'31 KB\', 2, \'Tetrahedra\', 60403, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 235076, 'I(3, 1, \'Disk\', \'214 Bytes\', 2, \'Tetrahedra\', 60403, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 4, 0, 542820, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 83793, false, 3, \'Matrix bandwidth\', 7.78616, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 542820, 'I(2, 1, \'Disk\', \'3.13 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176536, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.105305, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:49:17')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 150324, 'I(1, 2, \'Tetrahedra\', 104117, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 228280, 'I(2, 1, \'Disk\', \'38 KB\', 2, \'Tetrahedra\', 77639, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 270048, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 77639, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 6, 0, 650136, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 104607, false, 3, \'Matrix bandwidth\', 7.93324, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 650136, 'I(2, 1, \'Disk\', \'3.81 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176536, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.108313, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:49:35')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 7, 0, 7, 0, 168380, 'I(1, 2, \'Tetrahedra\', 127412, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 277612, 'I(2, 1, \'Disk\', \'46.5 KB\', 2, \'Tetrahedra\', 99980, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315640, 'I(3, 1, \'Disk\', \'300 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 4, 0, 8, 0, 823988, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 823988, 'I(2, 1, \'Disk\', \'4.71 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176536, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0112471, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 11:49:58')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:10:23')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 11:49:58')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:10:23')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259876, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316020, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 506476, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506476, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259264, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316008, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 559868, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 559868, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258432, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315884, 'I(3, 1, \'Disk\', \'13 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 550440, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 550440, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259468, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316472, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 569092, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 569092, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258032, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338492, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 1047788, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 12.6005, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1047788, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258584, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315964, 'I(3, 1, \'Disk\', \'18 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 556056, 'I(4, 1, \'Disk\', \'5 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 556056, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 258452, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316072, 'I(3, 1, \'Disk\', \'15 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 514296, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 514296, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 114.224%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176540, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 122.980%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260100, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315788, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 547756, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 547756, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 2.275GHz; S Matrix Error =  47.067%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259228, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316080, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 558140, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 558140, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 1.9125GHz; S Matrix Error =   1.105%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258444, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316188, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 552524, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 552524, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.6375GHz; S Matrix Error =   0.267%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259524, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316212, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 566344, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 566344, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 550MHz; S Matrix Error =   0.135%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315516, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 565184, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 565184, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 325MHz; S Matrix Error =   0.055%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260368, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316628, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 512164, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 512164, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 212.5MHz; S Matrix Error =   0.046%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258104, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315804, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 481444, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 481444, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 1.275GHz; S Matrix Error =   0.046%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260208, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315420, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 504832, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 504832, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.09375GHz; S Matrix Error =   0.152%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315868, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 555836, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 555836, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.003125GHz; S Matrix Error =   0.236%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260252, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 565520, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 565520, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.0484375GHz; S Matrix Error =   0.243%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260036, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315944, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 548528, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 548528, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.07109375GHz; S Matrix Error =   0.071%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258504, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316072, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 509768, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 509768, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.082421875GHz; S Matrix Error =   0.045%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260036, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315312, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 491612, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 491612, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.0880859375GHz; S Matrix Error =   0.017%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.08525390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258844, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316084, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 511620, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 511620, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.08525390625GHz; S Matrix Error =   0.010%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259136, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316164, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 551672, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 551672, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.81875GHz; S Matrix Error =   0.012%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259804, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316208, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 496244, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 496244, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.909375GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259872, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316796, 'I(3, 1, \'Disk\', \'15 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 533128, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 533128, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 32.5MHz; S Matrix Error =   0.006%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.45625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259228, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315804, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 497672, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 497672, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.45625GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258504, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315904, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 543484, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 543484, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.546875GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259016, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316304, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 507460, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507460, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 77.5MHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258460, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315732, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 508780, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 508780, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 88.75MHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.5921875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258152, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315948, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 501096, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 501096, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 2.5921875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59053413353338GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259900, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315884, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 576788, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 576788, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 2.59053413353338GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316400, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 493408, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 493408, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 2.61484375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.626171875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259144, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315964, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 554956, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 554956, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.626171875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259396, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315628, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 536740, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 536740, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259520, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315616, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 525920, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 525920, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258424, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 339284, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 640292, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 12.6005, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 640292, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258676, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316200, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 531452, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 531452, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 257880, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 339076, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 996560, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 12.6005, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 996560, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260032, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316324, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 486168, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 486168, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259076, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338548, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 985364, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 12.6005, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 985364, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260288, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316172, 'I(3, 1, \'Disk\', \'18 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 552348, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 552348, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176544, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259592, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315836, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 552956, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 552956, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176548, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259520, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315980, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 494204, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 494204, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176548, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260032, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315784, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 494360, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 494360, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176548, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258716, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315912, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 549004, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 549004, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176548, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258904, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 315804, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 497228, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 497228, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176548, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.97734375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260012, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316232, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 506952, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506952, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 2.97734375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176548, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 268.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260164, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316412, 'I(3, 1, \'Disk\', \'15 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 488080, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 488080, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 268.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176548, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 94.375MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258688, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315552, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 554500, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 554500, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 94.375MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176552, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 114.0625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258820, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316204, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 99980, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 486288, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131445, false, 3, \'Matrix bandwidth\', 8.05827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 486288, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 114.0625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 176552, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'172 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:14\', 1, \'Total Memory\', \'185 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:03\', 1, \'Average memory/process\', \'805 MB\', 1, \'Max memory/process\', \'805 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:10:23\', 1, \'Average memory/process\', \'1.02e+03 MB\', 1, \'Max memory/process\', \'1.02e+03 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 99980, false, 2, \'Max matrix size\', 131445, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 12:00:21\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
