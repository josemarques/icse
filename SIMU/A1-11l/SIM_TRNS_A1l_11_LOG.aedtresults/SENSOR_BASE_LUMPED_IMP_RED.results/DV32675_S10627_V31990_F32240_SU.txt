    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 100000000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 115392
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 321.000000
    MemoryTotalMB 833.000000
    NumMeshStatisticsTets 115392
    MemoryForSMatrixOnlyMB 603.324219
    LowFrequencyCutoff 2381589.369592
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 150038
    TotalModes 2
    NnzGrowthFactor 20.367186
