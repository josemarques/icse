$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 11:23:27')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:11:59')
			I(1, 'ComEngine Memory', '172 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 124 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 172 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 11:23:27')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:14')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 7, 0, 7, 0, 127240, 'I(1, 2, \'Tetrahedra\', 71652, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 172192, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 188164, 'I(2, 1, \'Disk\', \'4.81 KB\', 2, \'Tetrahedra\', 46851, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 115544, 'I(1, 2, \'Tetrahedra\', 71873, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 11:23:41')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:05')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:23:41')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 180184, 'I(2, 1, \'Disk\', \'23.6 KB\', 2, \'Tetrahedra\', 47036, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 205420, 'I(3, 1, \'Disk\', \'64.6 KB\', 2, \'Tetrahedra\', 47036, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 2, 0, 446940, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 67691, false, 3, \'Matrix bandwidth\', 7.60611, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 446940, 'I(2, 1, \'Disk\', \'6.41 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175800, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:23:50')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:15')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 137724, 'I(1, 2, \'Tetrahedra\', 85988, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 202012, 'I(2, 1, \'Disk\', \'31.3 KB\', 2, \'Tetrahedra\', 60434, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 234692, 'I(3, 1, \'Disk\', \'214 Bytes\', 2, \'Tetrahedra\', 60434, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 4, 0, 488916, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 83798, false, 3, \'Matrix bandwidth\', 7.78858, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 488916, 'I(2, 1, \'Disk\', \'3.14 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175800, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0986201, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:24:06')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:18')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 150860, 'I(1, 2, \'Tetrahedra\', 104121, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 228260, 'I(2, 1, \'Disk\', \'39.2 KB\', 2, \'Tetrahedra\', 77732, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 270020, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 77732, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 6, 0, 635848, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 104659, false, 3, \'Matrix bandwidth\', 7.93656, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 635848, 'I(2, 1, \'Disk\', \'3.82 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175800, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0525519, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:24:24')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 7, 0, 7, 0, 168148, 'I(1, 2, \'Tetrahedra\', 127442, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 277532, 'I(2, 1, \'Disk\', \'48 KB\', 2, \'Tetrahedra\', 100048, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315684, 'I(3, 1, \'Disk\', \'375 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 4, 0, 8, 0, 828732, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 828732, 'I(2, 1, \'Disk\', \'4.7 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175800, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0141601, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 11:24:47')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:10:39')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 11:24:47')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:10:39')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259936, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 486916, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 486916, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260020, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315840, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 554708, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 554708, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259180, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315208, 'I(3, 1, \'Disk\', \'16 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 541572, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 541572, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259660, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316064, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 474944, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 474944, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259592, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338524, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 12, 0, 995952, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 12.6005, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 995952, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259784, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315780, 'I(3, 1, \'Disk\', \'17 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 508660, 'I(4, 1, \'Disk\', \'6 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 508660, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258188, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316196, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 520944, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 520944, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 112.827%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175804, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 123.202%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259612, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316008, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 555840, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 555840, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 550MHz; S Matrix Error =  59.207%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259976, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316248, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 570456, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 570456, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.275GHz; S Matrix Error =  31.752%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259520, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315848, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 514192, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 514192, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.6375GHz; S Matrix Error =   0.677%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258724, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315920, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 555396, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 555396, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 1.9125GHz; S Matrix Error =   0.141%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259488, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316196, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 509536, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 509536, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 325MHz; S Matrix Error =   0.153%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260352, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316448, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 521768, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 521768, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 1.275GHz; S Matrix Error =   0.140%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258720, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315648, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 558168, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 558168, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.09375GHz; S Matrix Error =   0.550%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260008, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316152, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 527464, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 527464, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.003125GHz; S Matrix Error =   0.299%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259876, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316040, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 555324, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 555324, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.0484375GHz; S Matrix Error =   0.340%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260072, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316112, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 517944, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 517944, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.07109375GHz; S Matrix Error =   0.133%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260304, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315988, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 507448, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507448, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.082421875GHz; S Matrix Error =   0.073%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258988, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316060, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 514980, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 514980, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.0880859375GHz; S Matrix Error =   0.054%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.08525390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259524, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315876, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 563804, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 563804, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.08525390625GHz; Scattering matrix quantities converged; Passivity Error =   0.003227; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09857514378595GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258852, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316024, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 534712, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 534712, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.09857514378595GHz; Scattering matrix quantities converged; Passivity Error =   0.000594; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.78326806701675GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258884, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315956, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 508716, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 508716, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.78326806701675GHz; Scattering matrix quantities converged; Passivity Error =   0.000502; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175808, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.58378469617404GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258664, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315956, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 529428, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 529428, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.58378469617404GHz; Scattering matrix quantities converged; Passivity Error =   0.003302; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59653363340835GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259560, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316236, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 499140, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 499140, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 2.59653363340835GHz; Scattering matrix quantities converged; Passivity Error =   0.000587; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.57853513378345GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258904, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315964, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 523056, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 523056, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.57853513378345GHz; Scattering matrix quantities converged; Passivity Error =   0.009043; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.616032008002GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259632, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316716, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 564608, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 564608, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.616032008002GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259780, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316100, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 549332, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 549332, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 32.5MHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259540, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315816, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 524544, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 524544, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 77.5MHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60628282070518GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259596, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316352, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 532504, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 532504, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 2.60628282070518GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258428, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316300, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 570488, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 570488, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258680, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315872, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 521224, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 521224, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 212.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258668, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316464, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 515852, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 515852, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259852, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316032, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 477640, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 477640, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258968, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338836, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 575060, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 12.6005, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 575060, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259952, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316684, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 578452, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 578452, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258856, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 339280, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 1017852, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 12.6005, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1017852, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259052, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316312, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 505492, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 505492, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259552, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315732, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 528236, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 528236, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259004, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338376, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 1072704, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 12.6005, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1072704, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259624, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315828, 'I(3, 1, \'Disk\', \'19 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 554588, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 554588, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175812, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.89163403350838GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258688, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315528, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 541148, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 541148, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 2.89163403350838GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.94581701675419GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259020, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316364, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 506588, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506588, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 2.94581701675419GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258212, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315636, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 540060, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 540060, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 38.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259392, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315900, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 559324, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 559324, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 38.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.97290850837709GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259856, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316180, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 510376, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 510376, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 2.97290850837709GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260188, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315776, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 507352, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507352, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 94.375MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259008, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315740, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 507168, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507168, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 94.375MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 7.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259692, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315888, 'I(3, 1, \'Disk\', \'12 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 505008, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 505008, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 7.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 6.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260336, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316208, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100048, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 497632, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131500, false, 3, \'Matrix bandwidth\', 8.05941, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 497632, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 6.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175816, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'172 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:14\', 1, \'Total Memory\', \'184 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:05\', 1, \'Average memory/process\', \'809 MB\', 1, \'Max memory/process\', \'809 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:10:39\', 1, \'Average memory/process\', \'1.02 GB\', 1, \'Max memory/process\', \'1.02 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 100048, false, 2, \'Max matrix size\', 131500, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 11:35:26\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
