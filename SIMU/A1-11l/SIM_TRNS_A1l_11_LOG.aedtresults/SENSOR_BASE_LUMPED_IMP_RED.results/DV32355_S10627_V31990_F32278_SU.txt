    VersionID HFSS2019
    NumberFrequencies 1
    Frequency 3250000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 115183
    BasisOrder 0
    TREECOTREE 1
    MemoryMasterMB 322.000000
    MemoryTotalMB 941.093750
    NumMeshStatisticsTets 115183
    MemoryForSMatrixOnlyMB 456.012127
    LowFrequencyCutoff 3595822.431428
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 1
    MemorySMatrixOnlyAccurate 0
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 1
    NumKrylovVectors -1
    MatrixSize 149704
    TotalModes 2
