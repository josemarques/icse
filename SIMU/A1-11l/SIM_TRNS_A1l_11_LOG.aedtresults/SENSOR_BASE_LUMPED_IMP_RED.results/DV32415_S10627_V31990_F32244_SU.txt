    VersionID HFSS2019
    NumberFrequencies 1
    Frequency 1000000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 117672
    BasisOrder 0
    TREECOTREE 1
    MemoryMasterMB 315.000000
    MemoryTotalMB 955.539063
    NumMeshStatisticsTets 117672
    MemoryForSMatrixOnlyMB 453.850109
    LowFrequencyCutoff 3414070.806083
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 1
    MemorySMatrixOnlyAccurate 0
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 1
    NumKrylovVectors -1
    MatrixSize 152718
    TotalModes 2
