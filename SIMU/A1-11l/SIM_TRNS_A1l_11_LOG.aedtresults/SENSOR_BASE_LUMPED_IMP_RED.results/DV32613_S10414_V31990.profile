$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 10:52:29')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:06:11')
			I(1, 'ComEngine Memory', '164 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 22.500000%, 1 task, 2 cores, Free Disk Space: 61.8 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 154 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:52:29')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:14')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 128092, 'I(1, 2, \'Tetrahedra\', 71652, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 171156, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 190016, 'I(2, 1, \'Disk\', \'4.81 KB\', 2, \'Tetrahedra\', 46851, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 116132, 'I(1, 2, \'Tetrahedra\', 71873, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:52:43')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:06')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:52:43')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 180432, 'I(2, 1, \'Disk\', \'23.6 KB\', 2, \'Tetrahedra\', 47036, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 206164, 'I(3, 1, \'Disk\', \'64.6 KB\', 2, \'Tetrahedra\', 47036, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 425400, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 67691, false, 3, \'Matrix bandwidth\', 7.60611, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 1, 0, 425400, 'I(2, 1, \'Disk\', \'6.41 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 160860, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:52:53')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:16')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 137332, 'I(1, 2, \'Tetrahedra\', 85995, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 201968, 'I(2, 1, \'Disk\', \'31.3 KB\', 2, \'Tetrahedra\', 60442, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 234344, 'I(3, 1, \'Disk\', \'214 Bytes\', 2, \'Tetrahedra\', 60442, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 4, 0, 489028, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 83813, false, 3, \'Matrix bandwidth\', 7.7884, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 489028, 'I(2, 1, \'Disk\', \'3.14 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 160988, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0839482, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:53:09')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 151044, 'I(1, 2, \'Tetrahedra\', 104132, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 228428, 'I(2, 1, \'Disk\', \'37.1 KB\', 2, \'Tetrahedra\', 77716, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 270512, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 77716, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 6, 0, 668888, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 104658, false, 3, \'Matrix bandwidth\', 7.93554, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 668888, 'I(2, 1, \'Disk\', \'3.82 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 164188, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0563568, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:53:27')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 7, 0, 7, 0, 168028, 'I(1, 2, \'Tetrahedra\', 127448, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 277804, 'I(2, 1, \'Disk\', \'47.1 KB\', 2, \'Tetrahedra\', 100091, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315888, 'I(3, 1, \'Disk\', \'375 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 4, 0, 9, 0, 843088, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 843088, 'I(2, 1, \'Disk\', \'4.71 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 164192, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0154422, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:53:50')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:04:50')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 10:53:50')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:04:49')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260224, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316120, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 491960, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 491960, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259644, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316068, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 516928, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 516928, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259516, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316212, 'I(3, 1, \'Disk\', \'6 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 517432, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 517432, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259832, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316296, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 565332, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 565332, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258416, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338952, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 12, 0, 975300, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 12.5992, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 975300, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259460, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315748, 'I(3, 1, \'Disk\', \'18 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 529524, 'I(4, 1, \'Disk\', \'6 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 529524, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259932, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316700, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 541880, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 541880, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 115.155%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166364, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 124.041%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260228, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316616, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 524812, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 524812, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 550MHz; S Matrix Error =  54.414%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166828, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258808, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315976, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 549568, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 549568, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.275GHz; S Matrix Error =  32.341%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166828, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260192, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316308, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 3, 0, 7, 0, 507260, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507260, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.6375GHz; S Matrix Error =   0.635%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166828, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.45625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260224, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315780, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 497488, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 497488, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 2.45625GHz; S Matrix Error =   0.140%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166828, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259652, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316552, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 554800, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 554800, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.329%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166832, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258556, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315892, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 3, 0, 7, 0, 562744, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 562744, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.500%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166832, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258684, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315632, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 520296, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 520296, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.274%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166832, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258584, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315452, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 505388, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 505388, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.597%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166844, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259200, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316200, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 478632, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 478632, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.355%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166844, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258420, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316024, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 516084, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 516084, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.082421875GHz; S Matrix Error =   0.192%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166844, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258236, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315692, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 567732, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 567732, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0880859375GHz; S Matrix Error =   0.101%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166844, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259004, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316140, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 499872, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 499872, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.052%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166848, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260124, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316376, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 521888, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 521888, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; S Matrix Error =   0.028%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166848, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258036, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316012, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 520888, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 520888, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.81875GHz; S Matrix Error =   0.016%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167028, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260028, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316196, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 576800, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 576800, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 325MHz; Scattering matrix quantities converged; Passivity Error =   0.006474; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167032, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60778269567392GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Terminated abnormally')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
				$end 'ProfileGroup'
				ProfileFootnote('I(1, 0, \'Interpolating sweep did NOT converge\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
				ProfileFootnote('I(1, 0, \'Sweep Simulation FAILED\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'154 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:14\', 1, \'Total Memory\', \'186 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:06\', 1, \'Average memory/process\', \'823 MB\', 1, \'Max memory/process\', \'823 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:04:50\', 1, \'Average memory/process\', \'952 MB\', 1, \'Max memory/process\', \'952 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 100091, false, 2, \'Max matrix size\', 131536, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 10:58:40\', 1, \'Status\', \'Aborted\')', 1)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 10:59:41')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:11:55')
			I(1, 'ComEngine Memory', '165 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 124 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 155 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:59:42')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:14')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 7, 0, 7, 0, 127840, 'I(1, 2, \'Tetrahedra\', 71652, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 172224, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 189864, 'I(2, 1, \'Disk\', \'4.81 KB\', 2, \'Tetrahedra\', 46851, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 116028, 'I(1, 2, \'Tetrahedra\', 71873, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:59:56')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:07')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:59:56')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:07')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 1, 0, 1, 0, 180268, 'I(2, 1, \'Disk\', \'23.6 KB\', 2, \'Tetrahedra\', 47036, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 205688, 'I(3, 1, \'Disk\', \'64.6 KB\', 2, \'Tetrahedra\', 47036, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 432568, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 67691, false, 3, \'Matrix bandwidth\', 7.60611, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 1, 0, 432688, 'I(2, 1, \'Disk\', \'6.41 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 161116, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:00:03')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:16')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 137028, 'I(1, 2, \'Tetrahedra\', 85995, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 202176, 'I(2, 1, \'Disk\', \'31.3 KB\', 2, \'Tetrahedra\', 60442, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 2, 0, 234456, 'I(3, 1, \'Disk\', \'214 Bytes\', 2, \'Tetrahedra\', 60442, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 4, 0, 493336, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 83813, false, 3, \'Matrix bandwidth\', 7.7884, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 493336, 'I(2, 1, \'Disk\', \'3.14 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 161692, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0839482, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:00:20')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:19')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 150488, 'I(1, 2, \'Tetrahedra\', 104132, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 228260, 'I(2, 1, \'Disk\', \'37.1 KB\', 2, \'Tetrahedra\', 77716, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 269692, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 77716, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 6, 0, 667660, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 104658, false, 3, \'Matrix bandwidth\', 7.93554, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 667660, 'I(2, 1, \'Disk\', \'3.82 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 164612, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0563568, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 11:00:40')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 7, 0, 7, 0, 168200, 'I(1, 2, \'Tetrahedra\', 127448, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 277992, 'I(2, 1, \'Disk\', \'47.1 KB\', 2, \'Tetrahedra\', 100091, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316260, 'I(3, 1, \'Disk\', \'375 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 4, 0, 8, 0, 859060, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 859060, 'I(2, 1, \'Disk\', \'4.71 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 164640, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0154422, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 11:01:04')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:10:33')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 11:01:04')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:10:33')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259648, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316436, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 572500, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 572500, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260260, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316620, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 557436, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 557436, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260160, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315716, 'I(3, 1, \'Disk\', \'6 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 544036, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 544036, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258168, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315820, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 550764, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 550764, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260052, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338940, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 993576, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 12.5992, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 993576, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260028, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316480, 'I(3, 1, \'Disk\', \'18 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 554152, 'I(4, 1, \'Disk\', \'6 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 554152, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259772, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316196, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 562708, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 562708, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 115.155%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167212, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 124.041%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258576, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315728, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 517344, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 517344, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 550MHz; S Matrix Error =  54.414%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167532, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259348, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 3, 0, 7, 0, 507652, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507652, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.275GHz; S Matrix Error =  32.341%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167532, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258428, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315448, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 540204, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 540204, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.6375GHz; S Matrix Error =   0.635%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167532, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.45625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260124, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315868, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 507652, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 507652, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 2.45625GHz; S Matrix Error =   0.140%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167532, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259860, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316056, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 484252, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 484252, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.329%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167536, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259932, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316392, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 505100, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 505100, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.500%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167536, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259928, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315868, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 561664, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 561664, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.274%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167536, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259456, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315976, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 547224, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 547224, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.597%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167548, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 260236, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316316, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 564096, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 564096, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.355%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167548, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258456, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316588, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 517644, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 517644, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.082421875GHz; S Matrix Error =   0.192%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167548, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 260096, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315624, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 520132, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 520132, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0880859375GHz; S Matrix Error =   0.101%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167548, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259644, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315832, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 566484, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 566484, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.052%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167548, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258032, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315636, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 542992, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 542992, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; S Matrix Error =   0.028%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167552, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259576, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316652, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 511868, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 511868, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.81875GHz; S Matrix Error =   0.016%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167552, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259344, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316124, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 527168, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 527168, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 325MHz; Scattering matrix quantities converged; Passivity Error =   0.006456; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167552, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60778269567392GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258408, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315524, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 566752, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 566752, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.60778269567392GHz; Scattering matrix quantities converged; Passivity Error =   0.000473; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167556, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.62578119529882GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259332, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316300, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 506468, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506468, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 2.62578119529882GHz; Scattering matrix quantities converged; Passivity Error =   0.000580; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167560, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.58603450862716GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260188, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315968, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 496980, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 496980, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.58603450862716GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167560, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258660, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315392, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 506552, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 506552, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 32.5MHz; S Matrix Error =   0.003%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167560, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59690860215054GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258648, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315944, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 523080, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 523080, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 2.59690860215054GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167560, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259700, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316228, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 540344, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 540344, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 77.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167560, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258204, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315732, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 501220, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 501220, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 88.75MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167560, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260060, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316128, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 528820, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 528820, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 212.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167560, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 260060, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315744, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 529472, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 529472, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167564, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259764, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338628, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 584368, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 12.5992, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 584368, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167952, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315828, 'I(3, 1, \'Disk\', \'8 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 517016, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 517016, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168464, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:14')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258448, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 339152, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 12, 0, 988868, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 12.5992, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 988868, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168464, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259812, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315496, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 510060, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 510060, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168464, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259632, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315936, 'I(3, 1, \'Disk\', \'8 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 494604, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 494604, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168464, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260392, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316256, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 515916, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 515916, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168464, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258100, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315380, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 519440, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 519440, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168468, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 260164, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316072, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 498468, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 498468, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168468, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260316, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316228, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 499052, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 499052, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168468, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259996, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316112, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 518328, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 518328, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168648, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 2, 0, 2, 0, 260132, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 317216, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 545312, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 545312, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168648, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 259948, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315804, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 504244, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 504244, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168652, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259604, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316252, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 500508, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 500508, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168652, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258716, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 340000, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 12, 0, 1037232, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 12.5992, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1037232, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168656, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 268.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258872, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316380, 'I(3, 1, \'Disk\', \'24 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 520036, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 520164, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 268.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168656, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.28125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260180, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 338944, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 12, 0, 993464, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 12.5992, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 993464, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 1.28125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168656, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 114.0625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259996, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 316032, 'I(3, 1, \'Disk\', \'31 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 8, 0, 521820, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 521820, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 114.0625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168656, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61678194548637GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 260188, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 315828, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100091, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 4, 0, 7, 0, 552284, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131536, false, 3, \'Matrix bandwidth\', 8.05998, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 552284, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 2.61678194548637GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168656, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'155 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:14\', 1, \'Total Memory\', \'185 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:07\', 1, \'Average memory/process\', \'839 MB\', 1, \'Max memory/process\', \'839 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:10:33\', 1, \'Average memory/process\', \'1.01e+03 MB\', 1, \'Max memory/process\', \'1.01e+03 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 100091, false, 2, \'Max matrix size\', 131536, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 11:11:37\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
