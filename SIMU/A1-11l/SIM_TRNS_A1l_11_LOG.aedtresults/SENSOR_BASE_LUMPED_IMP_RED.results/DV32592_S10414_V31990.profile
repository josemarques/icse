$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 10:39:14')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:07:17')
			I(1, 'ComEngine Memory', '166 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Auto\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 158 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:39:14')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:13')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 7, 0, 7, 0, 127884, 'I(1, 2, \'Tetrahedra\', 71652, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 172704, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 189048, 'I(2, 1, \'Disk\', \'4.81 KB\', 2, \'Tetrahedra\', 46851, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 116052, 'I(1, 2, \'Tetrahedra\', 71873, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:39:28')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:02')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:39:28')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:07')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 180076, 'I(2, 1, \'Disk\', \'23.6 KB\', 2, \'Tetrahedra\', 47036, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 221396, 'I(3, 1, \'Disk\', \'64.6 KB\', 2, \'Tetrahedra\', 47036, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 4, 0, 474216, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 67691, false, 3, \'Matrix bandwidth\', 7.60611, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 474216, 'I(2, 1, \'Disk\', \'6.41 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 165424, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:39:36')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:14')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 4, 0, 4, 0, 137868, 'I(1, 2, \'Tetrahedra\', 85984, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 201900, 'I(2, 1, \'Disk\', \'31.3 KB\', 2, \'Tetrahedra\', 60432, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 255100, 'I(3, 1, \'Disk\', \'214 Bytes\', 2, \'Tetrahedra\', 60432, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 7, 0, 574420, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 83796, false, 3, \'Matrix bandwidth\', 7.78856, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 574420, 'I(2, 1, \'Disk\', \'3.14 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 165424, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.105671, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:39:50')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:18')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 150828, 'I(1, 2, \'Tetrahedra\', 104120, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 228412, 'I(2, 1, \'Disk\', \'37.9 KB\', 2, \'Tetrahedra\', 77739, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 3, 0, 298096, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 77739, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 8, 0, 734896, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 104672, false, 3, \'Matrix bandwidth\', 7.93602, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 734896, 'I(2, 1, \'Disk\', \'3.82 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168440, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0640595, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:40:09')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:21')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 7, 0, 7, 0, 167700, 'I(1, 2, \'Tetrahedra\', 127443, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 277696, 'I(2, 1, \'Disk\', \'47.9 KB\', 2, \'Tetrahedra\', 100100, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350536, 'I(3, 1, \'Disk\', \'525 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 3, 0, 12, 0, 942764, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 4, 0, 942764, 'I(2, 1, \'Disk\', \'4.71 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168560, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0121007, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:40:30')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:06:00')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep, Solving Distributed - up to 4 frequencies in parallel')
					I(1, 'Time', '08/09/2022 10:40:30')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(0, 'Terminated abnormally')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Solving with MPI (Intel MPI)\')', false, true)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'158 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:13\', 1, \'Total Memory\', \'185 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:02\', 1, \'Average memory/process\', \'921 MB\', 1, \'Max memory/process\', \'921 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:06:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 100100, false, 2, \'Max matrix size\', 131520, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 10:46:31\', 1, \'Status\', \'Aborted\')', 1)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 10:46:58')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:04:42')
			I(1, 'ComEngine Memory', '165 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 1 task, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 156 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:46:58')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:15')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 127148, 'I(1, 2, \'Tetrahedra\', 71652, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 172716, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 190388, 'I(2, 1, \'Disk\', \'4.81 KB\', 2, \'Tetrahedra\', 46851, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 116464, 'I(1, 2, \'Tetrahedra\', 71873, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:47:14')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:13')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:47:14')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 180196, 'I(2, 1, \'Disk\', \'23.6 KB\', 2, \'Tetrahedra\', 47036, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 222064, 'I(3, 1, \'Disk\', \'64.6 KB\', 2, \'Tetrahedra\', 47036, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 5, 0, 472764, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 67691, false, 3, \'Matrix bandwidth\', 7.60611, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 472764, 'I(2, 1, \'Disk\', \'6.41 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 163852, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:47:23')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:18')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 138232, 'I(1, 2, \'Tetrahedra\', 85984, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 201768, 'I(2, 1, \'Disk\', \'31.3 KB\', 2, \'Tetrahedra\', 60432, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 3, 0, 254132, 'I(3, 1, \'Disk\', \'214 Bytes\', 2, \'Tetrahedra\', 60432, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 6, 0, 559296, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 83796, false, 3, \'Matrix bandwidth\', 7.78856, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 559296, 'I(2, 1, \'Disk\', \'3.14 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 162200, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.105671, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:47:41')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:21')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 7, 0, 6, 0, 150184, 'I(1, 2, \'Tetrahedra\', 104120, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 228396, 'I(2, 1, \'Disk\', \'37.9 KB\', 2, \'Tetrahedra\', 77739, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 4, 0, 296904, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 77739, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 9, 0, 708484, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 104672, false, 3, \'Matrix bandwidth\', 7.93602, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 708484, 'I(2, 1, \'Disk\', \'3.82 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 165364, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0640595, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 10:48:03')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:24')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 168328, 'I(1, 2, \'Tetrahedra\', 127443, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 277576, 'I(2, 1, \'Disk\', \'47.9 KB\', 2, \'Tetrahedra\', 100100, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349612, 'I(3, 1, \'Disk\', \'525 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 3, 0, 13, 0, 943684, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 943684, 'I(2, 1, \'Disk\', \'4.71 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 165416, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0121007, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 10:48:27')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:03:12')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 10:48:27')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:03:12')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 259036, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350368, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 656124, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 656124, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 260012, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350132, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 11, 0, 640800, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 640800, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 258752, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349004, 'I(3, 1, \'Disk\', \'14 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 673868, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 673868, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 259768, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349644, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 596276, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 596276, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 260232, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 392420, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 15, 0, 1148032, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 12.6012, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1148032, 'I(2, 1, \'Disk\', \'4.68 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 259932, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349148, 'I(3, 1, \'Disk\', \'9 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 11, 0, 619004, 'I(4, 1, \'Disk\', \'5 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 619004, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:08')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258648, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350380, 'I(3, 1, \'Disk\', \'12 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 613524, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 613524, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 120.134%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 167704, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 124.443%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:08')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258416, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349676, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 11, 0, 645240, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 645240, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 550MHz; S Matrix Error =  50.892%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168356, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:08')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259708, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350000, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 11, 0, 676320, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 676320, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.275GHz; S Matrix Error =  33.121%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168356, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:08')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258172, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349772, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 627948, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 627948, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 1.9125GHz; S Matrix Error =   1.155%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168356, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:08')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259788, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349944, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 576912, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 576912, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 2.6375GHz; S Matrix Error =   0.586%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168356, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:08')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259480, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350040, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 655508, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 655508, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 2.81875GHz; S Matrix Error =   0.373%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168356, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 258344, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349988, 'I(3, 1, \'Disk\', \'12 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 11, 0, 623756, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 623756, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.413%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168368, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:09')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259356, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350684, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 636180, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 636180, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.184375GHz; S Matrix Error =   0.087%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168368, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:08')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259644, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350344, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 10, 0, 670956, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 670956, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.003125GHz; S Matrix Error =   0.309%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 168368, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:08')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 259508, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 350700, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 100100, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 3, 0, 11, 0, 670984, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 131520, false, 3, \'Matrix bandwidth\', 8.06108, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 670984, 'I(2, 1, \'Disk\', \'4.69 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.0484375GHz; S Matrix Error =   0.305%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 169312, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep did NOT converge\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'156 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:15\', 1, \'Total Memory\', \'186 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:13\', 1, \'Average memory/process\', \'922 MB\', 1, \'Max memory/process\', \'922 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:03:12\', 1, \'Average memory/process\', \'1.09 GB\', 1, \'Max memory/process\', \'1.09 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 100100, false, 2, \'Max matrix size\', 131520, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 10:51:40\', 1, \'Status\', \'Stopped Cleanly\')', 1)
	$end 'ProfileGroup'
$end 'Profile'
