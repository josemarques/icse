    VersionID HFSS2019
    NumberFrequencies 1
    Frequency 2113573893.473370
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 100109
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 258.000000
    MemoryTotalMB 690.000000
    NumMeshStatisticsTets 100109
    MemoryForSMatrixOnlyMB 381.207031
    LowFrequencyCutoff 3414070.806083
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 4
    NumKrylovVectors -1
    MatrixSize 131495
    TotalModes 2
