    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 100000000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 100100
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 294.000000
    MemoryTotalMB 760.000000
    NumMeshStatisticsTets 100100
    MemoryForSMatrixOnlyMB 640.746094
    LowFrequencyCutoff 2381589.369592
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 4
    NumKrylovVectors -1
    MatrixSize 131520
    TotalModes 2
    NnzGrowthFactor 19.519105
