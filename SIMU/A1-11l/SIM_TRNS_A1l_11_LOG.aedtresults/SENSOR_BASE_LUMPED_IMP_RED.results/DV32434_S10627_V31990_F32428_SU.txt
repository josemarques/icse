    VersionID HFSS2019
    NumberFrequencies 1
    Frequency 1278125000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 100150
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 268.000000
    MemoryTotalMB 671.000000
    NumMeshStatisticsTets 100150
    MemoryForSMatrixOnlyMB 382.781250
    LowFrequencyCutoff 3414070.806083
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 131525
    TotalModes 2
