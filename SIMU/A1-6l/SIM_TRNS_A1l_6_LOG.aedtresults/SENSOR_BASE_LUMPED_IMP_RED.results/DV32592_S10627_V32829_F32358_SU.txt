    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 2909375000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 137688
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 362.000000
    MemoryTotalMB 1071.000000
    NumMeshStatisticsTets 137688
    MemoryForSMatrixOnlyMB 646.203125
    LowFrequencyCutoff 2933065.461733
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 176473
    TotalModes 2
    NnzGrowthFactor 24.402494
