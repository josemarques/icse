$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 18:54:09')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:14:46')
			I(1, 'ComEngine Memory', '174 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 123 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 174 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 18:54:09')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:16')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 131964, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 2, 0, 2, 0, 175996, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 193244, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 3, 0, 3, 0, 119068, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 18:54:25')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:38')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:54:25')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 185380, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 212160, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 415148, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 1, 0, 415148, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178456, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:54:35')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 141288, 'I(1, 2, \'Tetrahedra\', 90326, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208452, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64237, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 243472, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64237, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 5, 0, 523128, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88787, false, 3, \'Matrix bandwidth\', 7.80373, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 523128, 'I(2, 1, \'Disk\', \'3.3 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178456, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.151692, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:54:52')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:20')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 155420, 'I(1, 2, \'Tetrahedra\', 109598, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 252164, 'I(2, 1, \'Disk\', \'43.5 KB\', 2, \'Tetrahedra\', 82769, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 2, 0, 281408, 'I(3, 1, \'Disk\', \'160 Bytes\', 2, \'Tetrahedra\', 82769, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 7, 0, 756640, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110978, false, 3, \'Matrix bandwidth\', 7.9561, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 756640, 'I(2, 1, \'Disk\', \'4.05 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178456, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0577389, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:55:13')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:26')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 172868, 'I(1, 2, \'Tetrahedra\', 134430, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 286456, 'I(2, 1, \'Disk\', \'54.1 KB\', 2, \'Tetrahedra\', 106771, false)', true, true)
				ProfileItem('Matrix Assembly', 3, 0, 4, 0, 330036, 'I(3, 1, \'Disk\', \'443 Bytes\', 2, \'Tetrahedra\', 106771, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 5, 0, 10, 0, 914440, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 139616, false, 3, \'Matrix bandwidth\', 8.08397, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 914440, 'I(2, 1, \'Disk\', \'5.04 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178456, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0258062, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 18:55:39')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 171748, 'I(1, 2, \'Tetrahedra\', 143359, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 299464, 'I(2, 1, \'Disk\', \'57.8 KB\', 2, \'Tetrahedra\', 115233, false)', true, true)
				ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347652, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 6, 0, 12, 0, 1008008, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 2, 0, 4, 0, 1008008, 'I(2, 1, \'Disk\', \'3.57 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178456, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.00452559, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 18:56:03')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:12:51')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 18:56:03')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:12:51')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283744, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347392, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 628216, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 628216, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284032, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347844, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 605752, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 605752, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283580, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372256, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 725344, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 12.6865, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 725344, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284484, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 371792, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 760432, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 12.6865, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 760432, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283684, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 6, 0, 372476, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 14, 0, 1197216, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 12.6865, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1197216, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284548, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347276, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 636628, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 636628, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284472, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 348232, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 613060, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 613060, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 161.893%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178460, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 143.409%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284524, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347720, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 611876, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 611876, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 550MHz; S Matrix Error =   6.286%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284720, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347644, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 600384, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 600384, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.275GHz; S Matrix Error =  16.928%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283824, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347492, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 635632, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 635632, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.6375GHz; S Matrix Error =   1.112%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284864, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347492, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 589988, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 589988, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 2.81875GHz; S Matrix Error =   0.459%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283428, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 348236, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 592736, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 592736, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.881%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284504, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347520, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 630544, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 630544, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.643%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284064, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347920, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 666904, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 666904, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.033%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284324, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347912, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 612540, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 612540, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.036%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284388, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346664, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 604348, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 604348, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.166%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.059765625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284588, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347652, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 607824, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 607824, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.059765625GHz; S Matrix Error =   0.148%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284068, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349128, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 631152, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 631152, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.082421875GHz; S Matrix Error =   0.121%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283356, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347848, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 628936, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 628936, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.051%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283424, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347496, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 600104, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 600104, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; S Matrix Error =   0.014%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.11640625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283620, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347112, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 619192, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 619192, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.11640625GHz; Scattering matrix quantities converged; Passivity Error =   0.000292; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59653363340835GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283232, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347564, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 630960, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 630960, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.59653363340835GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178464, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284716, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347328, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 671024, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 671024, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 325MHz; S Matrix Error =   0.004%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284160, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 371600, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 704324, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 12.6865, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 704324, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 32.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284592, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347360, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 628064, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 628064, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 77.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284204, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347144, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 629084, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 629084, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 212.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283428, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347400, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 611108, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 611108, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283840, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 348244, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 596908, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 596908, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282592, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347704, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 620864, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 620864, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283416, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347128, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 616876, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 616876, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.43576681670418GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283652, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347504, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 560632, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 560632, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 2.43576681670418GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60928257064266GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284868, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347208, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 623900, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 623900, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.60928257064266GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60290810202551GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283848, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347692, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 611928, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 611928, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 2.60290810202551GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.62339128532133GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284744, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347524, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 636612, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 636612, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.62339128532133GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284032, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 373064, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 719732, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 12.6865, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 719732, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284352, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 372500, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1203312, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 12.6865, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1203312, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284848, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347448, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 609604, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 609604, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283328, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347068, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 567556, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 567556, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283472, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372352, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 714544, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 12.6865, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 714544, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284872, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 371788, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 708276, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 12.6865, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 708276, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178468, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284140, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347292, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 622712, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 622712, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 114.0625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284048, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347384, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 624752, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 624752, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 114.0625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284200, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347008, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 635672, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 635672, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283932, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 348800, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 598196, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 598196, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284272, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347720, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 634912, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 634912, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.105078125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284576, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346836, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 9, 0, 595920, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 595920, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 2.105078125GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1107421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284424, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 349760, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 645896, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 645896, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 2.1107421875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.616336927982GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284352, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347676, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 587632, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 587632, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 2.616336927982GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284592, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 349720, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115233, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 602052, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149827, false, 3, \'Matrix bandwidth\', 8.11373, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 602052, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178472, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'174 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:16\', 1, \'Total Memory\', \'189 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:38\', 1, \'Average memory/process\', \'984 MB\', 1, \'Max memory/process\', \'984 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:12:51\', 1, \'Average memory/process\', \'1.15 GB\', 1, \'Max memory/process\', \'1.15 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 115233, false, 2, \'Max matrix size\', 149827, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 19:08:55\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
