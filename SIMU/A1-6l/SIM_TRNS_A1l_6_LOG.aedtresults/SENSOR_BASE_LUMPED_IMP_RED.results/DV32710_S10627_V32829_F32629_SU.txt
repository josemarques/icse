    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 775000000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 115302
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 321.000000
    MemoryTotalMB 844.000000
    NumMeshStatisticsTets 115302
    MemoryForSMatrixOnlyMB 596.855469
    LowFrequencyCutoff 2829202.987517
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 149926
    TotalModes 2
    NnzGrowthFactor 21.546491
