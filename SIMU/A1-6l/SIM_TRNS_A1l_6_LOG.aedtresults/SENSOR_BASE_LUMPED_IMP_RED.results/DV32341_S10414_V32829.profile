$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 15:40:22')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:12:45')
			I(1, 'ComEngine Memory', '171 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 1 task, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 171 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 15:40:23')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:15')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 132252, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 175920, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 195272, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 3, 0, 3, 0, 119096, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 15:40:38')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:24')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:40:38')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:07')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 1, 0, 1, 0, 185512, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 229740, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 5, 0, 506268, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 506268, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174996, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:40:46')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:15')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 140616, 'I(1, 2, \'Tetrahedra\', 90326, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208524, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64228, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 3, 0, 265608, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64228, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 7, 0, 595176, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88763, false, 3, \'Matrix bandwidth\', 7.80482, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 4, 0, 595176, 'I(2, 1, \'Disk\', \'3.3 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174996, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.182518, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:41:01')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:18')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 154376, 'I(1, 2, \'Tetrahedra\', 109600, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 251704, 'I(2, 1, \'Disk\', \'43.5 KB\', 2, \'Tetrahedra\', 82799, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 309908, 'I(3, 1, \'Disk\', \'285 Bytes\', 2, \'Tetrahedra\', 82799, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 9, 0, 840784, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110951, false, 3, \'Matrix bandwidth\', 7.9595, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 4, 0, 840784, 'I(2, 1, \'Disk\', \'4.05 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174996, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0521243, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:41:19')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:22')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 173064, 'I(1, 2, \'Tetrahedra\', 134444, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286284, 'I(2, 1, \'Disk\', \'54.1 KB\', 2, \'Tetrahedra\', 106871, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 366628, 'I(3, 1, \'Disk\', \'130 Bytes\', 2, \'Tetrahedra\', 106871, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 4, 0, 14, 0, 1057816, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Matrix size\', 139634, false, 3, \'Matrix bandwidth\', 8.08814, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 1057816, 'I(2, 1, \'Disk\', \'5.04 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174996, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0214599, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:41:41')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:21')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 170960, 'I(1, 2, \'Tetrahedra\', 142754, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 298532, 'I(2, 1, \'Disk\', \'57.4 KB\', 2, \'Tetrahedra\', 114759, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384392, 'I(3, 1, \'Disk\', \'225 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 4, 0, 16, 0, 1212140, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 1212140, 'I(2, 1, \'Disk\', \'3.5 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174996, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.00406415, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 15:42:03')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:11:05')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 15:42:03')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:11:05')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283432, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385960, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 734724, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 734724, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283096, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385540, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 751532, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 751532, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282936, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 431460, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 866608, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 12.6928, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 866608, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283860, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 431468, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 885060, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 12.6928, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 885060, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282132, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 432944, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 20, 0, 1320644, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 12.6928, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1320644, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282624, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385320, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 743864, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 743864, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283764, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384736, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 764808, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 764808, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 149.181%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174996, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error =  82.982%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283676, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385084, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 755680, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 755680, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 2.275GHz; S Matrix Error =   3.314%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175000, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282312, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385184, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 706920, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 706920, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.6375GHz; S Matrix Error =   2.726%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175000, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282804, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385264, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 801820, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 801820, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.81875GHz; S Matrix Error =   1.119%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175000, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282672, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385808, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 775576, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 775576, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 550MHz; S Matrix Error =   0.927%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175000, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283156, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385432, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 748104, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 748104, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.857%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175000, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283828, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384468, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 741352, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 741352, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.700%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175000, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283124, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384824, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 710104, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 710104, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.077%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175000, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282848, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385848, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 759148, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 759148, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.094%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175000, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281904, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385540, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 778604, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 778604, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.261%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283600, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384944, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 688628, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 688628, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.082421875GHz; S Matrix Error =   0.332%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283312, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385488, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 753264, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 753264, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0880859375GHz; S Matrix Error =   0.248%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283700, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385092, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 820808, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 820808, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.016%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283496, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385520, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 814456, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 814456, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; S Matrix Error =   0.011%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283312, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 386004, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 777720, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 777720, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 325MHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282340, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385680, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 751364, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 751364, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 212.5MHz; S Matrix Error =   0.005%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283512, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 431952, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 878632, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 12.6928, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 878632, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 32.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282964, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385188, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 774968, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 774968, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 77.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283248, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385352, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 755384, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 755384, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.909375GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61453213303326GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283092, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 384540, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 737084, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 737084, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.61453213303326GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.44476606651663GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282128, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384640, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 742380, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 742380, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 2.44476606651663GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281928, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 385860, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 787068, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 787068, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.52964909977494GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283832, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 389912, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 801276, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 801276, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 2.52964909977494GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61303225806452GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283260, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384356, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 787736, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 787736, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 2.61303225806452GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.57134067891973GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283240, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384468, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 739912, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 739912, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 2.57134067891973GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59218646849213GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283700, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385140, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 766036, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 766036, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.59218646849213GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283708, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 433508, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 835872, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 12.6928, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 835872, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283448, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 432392, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 21, 0, 1393780, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 12.6928, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1393780, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283168, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385584, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 761740, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 761740, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282488, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385544, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 761816, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 761816, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283472, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 431788, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 863348, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 12.6928, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 863348, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282324, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 7, 0, 432396, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 854888, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 12.6928, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 854888, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283264, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385476, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 773204, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 773204, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283732, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385236, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 745032, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 745032, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175004, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283804, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385068, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 765008, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 765008, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281752, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385120, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 726472, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 726472, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283552, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385080, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 715648, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 715648, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282356, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384984, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 731144, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 731144, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.4125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282324, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385388, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 760268, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 760268, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 1.4125GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.11640625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283252, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385460, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 733068, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 733068, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 2.11640625GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.105078125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283964, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 384376, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 752876, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 752876, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 2.105078125GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 114.0625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282112, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385284, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 726372, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 726372, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 114.0625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 107.03125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282660, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385444, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114759, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 730272, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149161, false, 3, \'Matrix bandwidth\', 8.11525, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 730272, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 107.03125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 175008, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'171 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:15\', 1, \'Total Memory\', \'191 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:24\', 1, \'Average memory/process\', \'1.16 GB\', 1, \'Max memory/process\', \'1.16 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:11:05\', 1, \'Average memory/process\', \'1.33 GB\', 1, \'Max memory/process\', \'1.33 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 114759, false, 2, \'Max matrix size\', 149161, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 15:53:08\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
