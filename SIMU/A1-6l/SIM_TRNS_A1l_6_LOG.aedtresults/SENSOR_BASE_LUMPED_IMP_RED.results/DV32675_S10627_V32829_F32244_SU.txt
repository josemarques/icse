    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 1000000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 125598
    BasisOrder 0
    TREECOTREE 1
    MemoryMasterMB 357.000000
    MemoryTotalMB 1302.679688
    NumMeshStatisticsTets 125598
    MemoryForSMatrixOnlyMB 793.305363
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 1
    MemorySMatrixOnlyAccurate 0
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 162177
    TotalModes 2
    NnzGrowthFactor 18.154102
