    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 1281250.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 137688
    BasisOrder 0
    TREECOTREE 1
    MemoryMasterMB 380.000000
    MemoryTotalMB 1435.820312
    NumMeshStatisticsTets 137688
    MemoryForSMatrixOnlyMB 775.094765
    LowFrequencyCutoff 2991258.748050
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 1
    MemorySMatrixOnlyAccurate 0
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 176437
    TotalModes 2
    NnzGrowthFactor 20.138199
