$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 17:34:09')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:14:51')
			I(1, 'ComEngine Memory', '174 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 45.000000%, 1 task, 2 cores, Free Disk Space: 123 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 174 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 17:34:09')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:15')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 131448, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 2, 0, 1, 0, 176076, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 193232, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 3, 0, 3, 0, 119404, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 17:34:25')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:36')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 17:34:25')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:09')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 1, 0, 1, 0, 185452, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 1, 0, 212668, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 1, 0, 3, 0, 415232, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 415232, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178000, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 17:34:34')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:17')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 140472, 'I(1, 2, \'Tetrahedra\', 90327, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208580, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64217, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 2, 0, 243560, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64217, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 2, 0, 5, 0, 554172, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88763, false, 3, \'Matrix bandwidth\', 7.80384, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 554172, 'I(2, 1, \'Disk\', \'3.3 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178000, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.164971, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 17:34:52')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:20')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 155300, 'I(1, 2, \'Tetrahedra\', 109596, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 251936, 'I(2, 1, \'Disk\', \'43.5 KB\', 2, \'Tetrahedra\', 82763, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 281500, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 82763, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 3, 0, 7, 0, 765968, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110966, false, 3, \'Matrix bandwidth\', 7.95664, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 2, 0, 765968, 'I(2, 1, \'Disk\', \'4.05 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178000, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0564655, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 17:35:12')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:23')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 173420, 'I(1, 2, \'Tetrahedra\', 134430, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286400, 'I(2, 1, \'Disk\', \'53 KB\', 2, \'Tetrahedra\', 106745, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 3, 0, 331224, 'I(3, 1, \'Disk\', \'755 Bytes\', 2, \'Tetrahedra\', 106745, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 5, 0, 10, 0, 933468, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 139597, false, 3, \'Matrix bandwidth\', 8.0832, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 3, 0, 933468, 'I(2, 1, \'Disk\', \'5.03 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178000, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0248265, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 17:35:36')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:25')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 171600, 'I(1, 2, \'Tetrahedra\', 143194, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 299244, 'I(2, 1, \'Disk\', \'57.1 KB\', 2, \'Tetrahedra\', 115076, false)', true, true)
				ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346932, 'I(3, 1, \'Disk\', \'363 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS2', 6, 0, 12, 0, 1052064, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 2, 0, 4, 0, 1052064, 'I(2, 1, \'Disk\', \'3.55 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178000, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.00520731, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 17:36:02')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:12:58')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 17:36:02')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:12:58')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284108, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347132, 'I(3, 1, \'Disk\', \'9 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 610044, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 610044, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284364, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346968, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 602168, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 602168, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284636, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 371416, 'I(3, 1, \'Disk\', \'16 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 742884, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 742884, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283468, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372144, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 755556, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 755556, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:17')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283928, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 374028, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 16, 0, 1252620, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1252620, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282912, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346728, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 590148, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 590148, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284340, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346848, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 640920, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 640920, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 146.294%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178004, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error = 155.321%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282220, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346820, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 6, 0, 11, 0, 621080, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 621080, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 2.275GHz; S Matrix Error =  10.677%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283680, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347304, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 606576, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 606576, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.6375GHz; S Matrix Error =   5.597%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284632, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347560, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 572508, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 572508, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.81875GHz; S Matrix Error =   0.986%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283696, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347064, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 562552, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 562552, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 1.9125GHz; S Matrix Error =   1.110%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283888, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347540, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 568792, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 568792, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 2.09375GHz; S Matrix Error =   1.005%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283152, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347204, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 620452, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 620452, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.003125GHz; S Matrix Error =   0.335%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283632, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347136, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 660276, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 660276, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.0484375GHz; S Matrix Error =   0.477%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283184, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346676, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 643356, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 643356, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.07109375GHz; S Matrix Error =   0.527%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284020, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347060, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 605908, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 605908, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.082421875GHz; S Matrix Error =   0.604%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283928, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347132, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 616204, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 616204, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.0880859375GHz; S Matrix Error =   0.188%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283184, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347284, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 627912, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 627912, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.184375GHz; S Matrix Error =   0.047%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284584, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347660, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 634988, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 634988, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 550MHz; S Matrix Error =   0.023%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283560, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347508, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 608784, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 608784, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 325MHz; Scattering matrix quantities converged; Passivity Error =   0.003570; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.10382470617654GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283896, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347004, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 581704, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 581704, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.10382470617654GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284212, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372288, 'I(3, 1, \'Disk\', \'18 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 690556, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 690556, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 32.5MHz; S Matrix Error =   0.004%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.45625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284380, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347580, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 584160, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 584160, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.45625GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61303225806452GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283552, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347140, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 632384, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 632384, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 2.61303225806452GHz; Scattering matrix quantities converged; Passivity Error =   0.003572; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61753188297074GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284176, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347496, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 650432, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 650432, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.61753188297074GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61528207051763GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283748, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347224, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 594804, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 594804, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.61528207051763GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284280, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346780, 'I(3, 1, \'Disk\', \'13 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 679580, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 679580, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 77.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.53464112903226GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283864, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347040, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 580676, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 580676, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 2.53464112903226GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282736, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347216, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 618616, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 618616, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283608, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346808, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 653452, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 653452, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 212.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283724, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346200, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 655952, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 655952, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284068, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372656, 'I(3, 1, \'Disk\', \'18 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 14, 0, 732544, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 732544, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284052, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372828, 'I(3, 1, \'Disk\', \'4 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 16, 0, 1198104, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1198104, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284500, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347312, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 629616, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 629616, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282572, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372568, 'I(3, 1, \'Disk\', \'11 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 7, 0, 13, 0, 762240, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 762240, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284160, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 371960, 'I(3, 1, \'Disk\', \'8 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 741620, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 741620, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283916, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347296, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 636672, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 636672, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178008, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283280, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347184, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 577084, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 577084, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178012, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283352, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347620, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 599760, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 599760, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178012, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284288, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347500, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 569564, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 569564, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178012, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 284464, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346856, 'I(3, 1, \'Disk\', \'16 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 593484, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 593484, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.57383669354839GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283608, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 346928, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 633764, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 633764, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 2.57383669354839GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59343447580645GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284120, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346416, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 592368, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 592368, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 2.59343447580645GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60323336693549GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282800, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 3, 0, 347308, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 644968, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 644968, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 2.60323336693549GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283544, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 373288, 'I(3, 1, \'Disk\', \'33 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1205076, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1205076, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283896, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 346792, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 10, 0, 593256, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 593256, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284308, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 4, 0, 347620, 'I(3, 1, \'Disk\', \'7 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 577552, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 577552, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.28125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 284460, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 5, 0, 372620, 'I(3, 1, \'Disk\', \'16 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 8, 0, 15, 0, 1199028, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 12.6827, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1199028, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 1.28125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283752, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 347144, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 115076, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS2', 5, 0, 11, 0, 649608, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 149651, false, 3, \'Matrix bandwidth\', 8.11202, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 649608, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 178016, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'174 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:15\', 1, \'Total Memory\', \'189 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:36\', 1, \'Average memory/process\', \'1 GB\', 1, \'Max memory/process\', \'1 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:12:58\', 1, \'Average memory/process\', \'1.19 GB\', 1, \'Max memory/process\', \'1.19 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 2, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 115076, false, 2, \'Max matrix size\', 149651, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 17:49:00\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
