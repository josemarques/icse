    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 1281250.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 136838
    BasisOrder 0
    TREECOTREE 1
    MemoryMasterMB 378.000000
    MemoryTotalMB 1394.683594
    NumMeshStatisticsTets 136838
    MemoryForSMatrixOnlyMB 784.467392
    LowFrequencyCutoff 2813494.732970
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 1
    MemorySMatrixOnlyAccurate 0
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 175481
    TotalModes 2
    NnzGrowthFactor 19.272236
