    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 1562500.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 135523
    BasisOrder 0
    TREECOTREE 1
    MemoryMasterMB 376.000000
    MemoryTotalMB 1378.859375
    NumMeshStatisticsTets 135523
    MemoryForSMatrixOnlyMB 812.048771
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 1
    MemorySMatrixOnlyAccurate 0
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 173950
    TotalModes 2
    NnzGrowthFactor 18.770163
