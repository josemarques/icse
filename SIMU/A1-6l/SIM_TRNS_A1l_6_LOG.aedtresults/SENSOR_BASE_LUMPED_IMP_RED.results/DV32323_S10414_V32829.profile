$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 15:25:20')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:15:01')
			I(1, 'ComEngine Memory', '171 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 1 task, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 171 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 15:25:20')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:15')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 132176, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 176776, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 193716, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 2, 0, 2, 0, 118352, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 15:25:36')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:30')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:25:36')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:07')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 1, 0, 1, 0, 185448, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 230128, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 5, 0, 479792, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 479792, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174828, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:25:43')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:15')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 140568, 'I(1, 2, \'Tetrahedra\', 90327, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208504, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64219, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 3, 0, 266300, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64219, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 7, 0, 606980, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88755, false, 3, \'Matrix bandwidth\', 7.80456, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 4, 0, 606980, 'I(2, 1, \'Disk\', \'3.3 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174828, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.18736, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:25:58')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:18')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 155584, 'I(1, 2, \'Tetrahedra\', 109597, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 251812, 'I(2, 1, \'Disk\', \'43.5 KB\', 2, \'Tetrahedra\', 82817, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 309576, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 82817, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 9, 0, 804808, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110962, false, 3, \'Matrix bandwidth\', 7.96017, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 4, 0, 804808, 'I(2, 1, \'Disk\', \'4.06 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174828, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0452721, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:26:16')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:21')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 173460, 'I(1, 2, \'Tetrahedra\', 134445, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286236, 'I(2, 1, \'Disk\', \'54.5 KB\', 2, \'Tetrahedra\', 106848, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 366292, 'I(3, 1, \'Disk\', \'630 Bytes\', 2, \'Tetrahedra\', 106848, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 4, 0, 14, 0, 1077944, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Matrix size\', 139617, false, 3, \'Matrix bandwidth\', 8.08708, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 1077944, 'I(2, 1, \'Disk\', \'5.04 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174828, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0212389, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:26:38')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:27')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 10, 0, 10, 0, 197700, 'I(1, 2, \'Tetrahedra\', 163948, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 330336, 'I(2, 1, \'Disk\', \'63.3 KB\', 2, \'Tetrahedra\', 135312, false)', true, true)
				ProfileItem('Matrix Assembly', 3, 0, 6, 0, 433764, 'I(3, 1, \'Disk\', \'1.03 KB\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 5, 0, 19, 0, 1316124, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 6, 0, 1316124, 'I(2, 1, \'Disk\', \'5.99 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174828, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0110445, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 15:27:06')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:13:15')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 15:27:06')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:13:15')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317388, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434696, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 976624, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 976624, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315856, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 434716, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 844072, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 844072, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315732, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 5, 0, 8, 0, 489076, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 23, 0, 1027716, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1027716, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 3, 0, 316024, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 5, 0, 8, 0, 488892, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 24, 0, 1055448, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1055448, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:17')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317688, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 8, 0, 489512, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 8, 0, 27, 0, 1626836, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1626836, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317012, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 433736, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 846996, 'I(4, 1, \'Disk\', \'7 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 846996, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 317680, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 436524, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 903508, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 903508, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 141.477%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174832, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error =  71.033%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 5, 0, 5, 0, 317212, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434376, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 876060, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 876060, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 2.275GHz; S Matrix Error =   3.020%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317408, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435412, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 891600, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 891600, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.6375GHz; S Matrix Error =   2.338%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 5, 0, 5, 0, 317116, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 434840, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 826000, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 826000, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.81875GHz; S Matrix Error =   1.142%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317524, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434640, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 846540, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 846540, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 550MHz; S Matrix Error =   0.905%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315756, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435228, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 888212, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 888212, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.833%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317492, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435080, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 854956, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 854956, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.653%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316916, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 823496, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 823496, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.061%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316272, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434800, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 873948, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 873948, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.054%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316376, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435304, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 894236, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 894236, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.123%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317660, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435484, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 836624, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 836624, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.082421875GHz; S Matrix Error =   0.220%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 316080, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 434764, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 891416, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 891416, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0880859375GHz; S Matrix Error =   0.077%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316232, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435120, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 946300, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 946300, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.020%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316404, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435136, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 835404, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 835404, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; S Matrix Error =   0.011%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 3, 0, 316448, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434664, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 858036, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 858036, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 325MHz; Scattering matrix quantities converged; Passivity Error =   0.000130; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.10457464366092GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317752, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435884, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 933524, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 933524, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.10457464366092GHz; Scattering matrix quantities converged; Passivity Error =   0.000199; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6107824456114GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316532, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435376, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 834448, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 834448, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.6107824456114GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174836, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317684, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 8, 0, 490572, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 23, 0, 1025748, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1025748, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 32.5MHz; S Matrix Error =   0.004%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6241412228057GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 316952, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 436452, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 932904, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 932904, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.6241412228057GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61746183420855GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 316880, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434864, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 927960, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 927960, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.61746183420855GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61412213990998GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315736, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 436408, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 886608, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 886608, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 2.61412213990998GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317556, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 434684, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 890416, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 890416, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 77.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317412, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434976, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 849844, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 849844, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317516, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435440, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 909592, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 909592, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 212.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315568, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435184, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 861400, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 861400, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315756, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 491180, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 23, 0, 1095828, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1095828, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316124, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 8, 0, 491560, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 8, 0, 27, 0, 1643596, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1643596, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 317484, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435260, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 893820, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 893820, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315652, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 490552, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 23, 0, 1104748, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1104748, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315652, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 8, 0, 489816, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 24, 0, 1033608, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1033608, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316272, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 434988, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 928808, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 928808, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316688, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434996, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 858712, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 858712, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.9546875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316080, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434440, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 880832, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 880832, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 2.9546875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315888, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434244, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 906624, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 906624, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174840, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 316496, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434632, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 808856, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 808856, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174844, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 381.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315368, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 7, 0, 434892, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 935176, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 935176, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 381.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174844, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:14')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 5, 0, 5, 0, 317012, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 434916, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 19, 0, 859648, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 859648, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174844, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315868, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 491196, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 8, 0, 27, 0, 1660344, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1660344, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174844, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 7.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315576, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 8, 0, 489880, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 23, 0, 1101532, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1101532, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 7.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174844, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 6.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 317464, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 490208, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 23, 0, 1064412, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 12.7952, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1064412, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 6.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174844, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315732, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 435380, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 839356, 'I(4, 1, \'Disk\', \'5 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 839356, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174844, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.4428912228057GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 316324, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 434620, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 891856, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 891856, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 2.4428912228057GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174844, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.52683683420855GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 315908, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 435184, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 135312, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 20, 0, 882204, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 173559, false, 3, \'Matrix bandwidth\', 8.18316, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 882204, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 2.52683683420855GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 174848, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'171 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:15\', 1, \'Total Memory\', \'189 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:30\', 1, \'Average memory/process\', \'1.26 GB\', 1, \'Max memory/process\', \'1.26 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:13:15\', 1, \'Average memory/process\', \'1.58 GB\', 1, \'Max memory/process\', \'1.58 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 135312, false, 2, \'Max matrix size\', 173559, false, 1, \'Matrix bandwidth\', \'8.2\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 15:40:21\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
