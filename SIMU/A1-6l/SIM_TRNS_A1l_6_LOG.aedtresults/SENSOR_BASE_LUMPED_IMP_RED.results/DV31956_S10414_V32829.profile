$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:34:42')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:04:41')
			I(1, 'ComEngine Memory', '164 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 2 tasks, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 156 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:34:42')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:02:12')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh TAU4', 48, 0, 53, 0, 517000, 'I(1, 2, \'Tetrahedra\', 144104, false)', true, true)
			ProfileItem('Mesh Post (TAU)', 9, 0, 9, 0, 517000, 'I(1, 2, \'Tetrahedra\', 57856, false)', true, true)
			ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 132104, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 1, 0, 1, 0, 178380, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 193960, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 3, 0, 3, 0, 118120, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:36:55')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:46')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 14:36:55')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:08')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 185380, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 229232, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 5, 0, 498520, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 498520, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 163264, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 14:37:03')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:20')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 141096, 'I(1, 2, \'Tetrahedra\', 90326, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208448, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64221, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 3, 0, 266168, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64221, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 7, 0, 609572, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88761, false, 3, \'Matrix bandwidth\', 7.80444, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 4, 0, 609572, 'I(2, 1, \'Disk\', \'3.3 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 163544, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.181591, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 14:37:24')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:24')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 7, 0, 7, 0, 155336, 'I(1, 2, \'Tetrahedra\', 109597, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 251840, 'I(2, 1, \'Disk\', \'43.1 KB\', 2, \'Tetrahedra\', 82844, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 309664, 'I(3, 1, \'Disk\', \'280 Bytes\', 2, \'Tetrahedra\', 82844, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 9, 0, 789088, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110983, false, 3, \'Matrix bandwidth\', 7.96047, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 4, 0, 789088, 'I(2, 1, \'Disk\', \'4.06 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166880, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0491755, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 14:37:48')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:25')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 173248, 'I(1, 2, \'Tetrahedra\', 134454, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286272, 'I(2, 1, \'Disk\', \'51.8 KB\', 2, \'Tetrahedra\', 106902, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 5, 0, 366664, 'I(3, 1, \'Disk\', \'480 Bytes\', 2, \'Tetrahedra\', 106902, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 3, 0, 13, 0, 1047056, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Matrix size\', 139658, false, 3, \'Matrix bandwidth\', 8.08816, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 1047056, 'I(2, 1, \'Disk\', \'5.04 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166896, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0241426, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 14:38:14')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:25')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 170788, 'I(1, 2, \'Tetrahedra\', 142416, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 298032, 'I(2, 1, \'Disk\', \'56.7 KB\', 2, \'Tetrahedra\', 114421, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385120, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 4, 0, 15, 0, 1204528, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 1204528, 'I(2, 1, \'Disk\', \'3.45 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 166908, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.00593731, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:38:41')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:42')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep, Solving Distributed - up to 2 frequencies in parallel')
					I(1, 'Time', '08/09/2022 14:38:41')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(0, 'Terminated abnormally')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Solving with MPI (Intel MPI)\')', false, true)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'156 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:02:12\', 1, \'Total Memory\', \'505 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:46\', 1, \'Average memory/process\', \'1.15 GB\', 1, \'Max memory/process\', \'1.15 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:42\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 114421, false, 2, \'Max matrix size\', 148756, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:39:24\', 1, \'Status\', \'Aborted\')', 1)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:40:17')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:01:29')
			I(1, 'ComEngine Memory', '153 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 2 tasks, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 144 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:40:19')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:27')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep, Solving Distributed - up to 2 frequencies in parallel')
					I(1, 'Time', '08/09/2022 14:40:19')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(0, 'Terminated abnormally')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Solving with MPI (Intel MPI)\')', false, true)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'144 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:01:27\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:41:46\', 1, \'Status\', \'Aborted\')', 1)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:42:06')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:02:05')
			I(1, 'ComEngine Memory', '153 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Auto\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 144 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:42:08')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:02:02')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep, Solving Distributed - up to 4 frequencies in parallel')
					I(1, 'Time', '08/09/2022 14:42:08')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(0, 'Terminated abnormally')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Solving with MPI (Intel MPI)\')', false, true)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'144 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:02:02\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:44:11\', 1, \'Status\', \'Aborted\')', 1)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:44:29')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:00:29')
			I(1, 'ComEngine Memory', '154 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 1 task, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 144 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:44:32')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:26')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 14:44:32')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(0, 'Terminated abnormally')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283308, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384756, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					$begin 'ProfileGroup'
						MajorVer=2022
						MinorVer=2
						Name='Solution Process'
						$begin 'StartInfo'
							I(1, 'Start Time', '08/09/2022 14:44:39')
							I(1, 'Host', 'mrqs-srv-0')
							I(1, 'Processor', '24')
							I(1, 'OS', 'Linux 5.15.0-43-generic')
							I(1, 'Product', 'HFSS Version 2022.2.0')
						$end 'StartInfo'
						$begin 'TotalInfo'
							I(0, 'Terminated abnormally')
						$end 'TotalInfo'
						GroupOptions=8
						TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 1 task, 4 cores, Free Disk Space: 247 GB\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
						ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 144 M\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
						ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
						$begin 'ProfileGroup'
							MajorVer=2022
							MinorVer=2
							Name='Frequency Sweep'
							$begin 'StartInfo'
								I(1, 'Time', '08/09/2022 14:44:41')
							$end 'StartInfo'
							$begin 'TotalInfo'
								I(0, 'Terminated abnormally')
							$end 'TotalInfo'
							GroupOptions=4
							TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
							ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
							$begin 'ProfileGroup'
								MajorVer=2022
								MinorVer=2
								Name='Solution - Sweep'
								$begin 'StartInfo'
									I(0, 'Interpolating HFSS Frequency Sweep')
									I(1, 'Time', '08/09/2022 14:44:41')
								$end 'StartInfo'
								$begin 'TotalInfo'
									I(0, 'Terminated abnormally')
								$end 'TotalInfo'
								GroupOptions=4
								TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
								ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
								ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
								ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
								$begin 'ProfileGroup'
									MajorVer=2022
									MinorVer=2
									Name='Frequency - 100MHz'
									$begin 'StartInfo'
										I(0, 'mrqs-srv-0')
									$end 'StartInfo'
									$begin 'TotalInfo'
										I(0, 'Elapsed time : 00:00:10')
									$end 'TotalInfo'
									GroupOptions=0
									TaskDataOptions('CPU Time'=8, 'Real Time'=8)
									ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
									ProfileItem('Solver DCS4', 4, 0, 14, 0, 772544, 'I(4, 1, \'Disk\', \'764 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
									ProfileItem('Field Recovery', 0, 0, 0, 0, 772544, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
								$end 'ProfileGroup'
								ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
								$begin 'ProfileGroup'
									MajorVer=2022
									MinorVer=2
									Name='Frequency - 55MHz'
									$begin 'StartInfo'
										I(0, 'mrqs-srv-0')
									$end 'StartInfo'
									$begin 'TotalInfo'
										I(0, 'Terminated abnormally')
									$end 'TotalInfo'
									GroupOptions=0
									TaskDataOptions('CPU Time'=8, 'Real Time'=8)
									ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
									ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283144, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
									ProfileItem('Matrix Assembly', 2, 0, 6, 0, 384660, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
								$end 'ProfileGroup'
							$end 'ProfileGroup'
						$end 'ProfileGroup'
						ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:44:58\', 1, \'Status\', \'Aborted\')', 1)
						ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:45:01\', 1, \'Status\', \'Aborted\')', 1)
					$end 'ProfileGroup'
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283156, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384160, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 736404, 'I(4, 1, \'Disk\', \'764 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 736404, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 784012, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 784012, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'144 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:00:26\', 1, \'Average memory/process\', \'766 MB\', 1, \'Max memory/process\', \'766 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 114421, false, 2, \'Max matrix size\', 148756, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
		$end 'ProfileGroup'
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Simulation Summary'
		$begin 'StartInfo'
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(0, ' ')
		$end 'TotalInfo'
		GroupOptions=0
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'144 MB\')', false, true)
		ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
		ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:00:20\', 1, \'Average memory/process\', \'719 MB\', 1, \'Max memory/process\', \'719 MB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
		ProfileFootnote('I(3, 2, \'Max solved tets\', 114421, false, 2, \'Max matrix size\', 148756, false, 1, \'Matrix bandwidth\', \'8.1\')', 0)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:45:35')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:00:35')
			I(1, 'ComEngine Memory', '152 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 4 tasks, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 144 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:45:37')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:33')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep, Solving Distributed - up to 4 frequencies in parallel')
					I(1, 'Time', '08/09/2022 14:45:37')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(0, 'Terminated abnormally')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Solving with MPI (Intel MPI)\')', false, true)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'144 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:33\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:46:11\', 1, \'Status\', \'Aborted\')', 1)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:46:21')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:01:08')
			I(1, 'ComEngine Memory', '153 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 2 tasks, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 144 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:46:23')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:05')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep, Solving Distributed - up to 2 frequencies in parallel')
					I(1, 'Time', '08/09/2022 14:46:23')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(0, 'Terminated abnormally')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Solving with MPI (Intel MPI)\')', false, true)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'144 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:01:05\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:47:29\', 1, \'Status\', \'Aborted\')', 1)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:47:41')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:11:13')
			I(1, 'ComEngine Memory', '156 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 1 task, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 143 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:47:44')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:11:11')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 14:47:44')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:11:11')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282548, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 386900, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 804288, 'I(4, 1, \'Disk\', \'764 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 804288, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282524, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 383520, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 777220, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 777220, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282456, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 432232, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 890780, 'I(4, 1, \'Disk\', \'3 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 890780, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281436, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 430708, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 909588, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 909588, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:14')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282532, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 7, 0, 433408, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 22, 0, 1383200, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1383200, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283192, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384752, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 761416, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 761416, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283068, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 383552, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 773776, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 773776, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 136.675%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 157520, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error =  66.160%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282596, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385112, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 781252, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 781252, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 2.275GHz; S Matrix Error =   3.140%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 157988, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282360, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384268, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 771888, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 771888, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.6375GHz; S Matrix Error =   2.186%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 157988, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282824, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384996, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 741776, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 741776, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.81875GHz; S Matrix Error =   1.186%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 157992, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282244, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 385316, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 738604, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 738604, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 550MHz; S Matrix Error =   0.954%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158004, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282640, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 383812, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 691980, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 691980, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.769%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158008, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282812, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384628, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 743876, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 743876, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.635%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158008, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283008, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 385152, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 744856, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 744856, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.093%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158008, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282296, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384544, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 768024, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 768024, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.138%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158020, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283156, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384432, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 756620, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 756620, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.599%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158020, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283048, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384340, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 740460, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 740460, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.082421875GHz; S Matrix Error =   0.968%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158020, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283240, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384956, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 738360, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 738360, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0880859375GHz; S Matrix Error =   0.092%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158020, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282688, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384724, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 696760, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 696760, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.013%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158020, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281300, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384904, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 762728, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 762728, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; S Matrix Error =   0.010%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158020, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 325MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282748, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384832, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 731024, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 731024, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 325MHz; Scattering matrix quantities converged; Passivity Error =   0.000972; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158020, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.72102325581395GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282324, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384384, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 715252, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 715252, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.72102325581395GHz; Scattering matrix quantities converged; Passivity Error =   0.000577; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158024, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61303225806452GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283228, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384688, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 761468, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 761468, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.61303225806452GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158028, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281532, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 431648, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 916988, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 916988, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 32.5MHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158028, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.44401612903226GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283496, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384888, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 770800, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 770800, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 2.44401612903226GHz; S Matrix Error =   0.002%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158028, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283176, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 383972, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 748316, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 748316, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 77.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158028, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281700, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384608, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 783040, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 783040, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158236, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 212.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282388, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384268, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 752584, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 752584, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 212.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158236, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 156.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283276, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384704, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 745588, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 745588, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 156.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158236, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 128.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281896, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384140, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 760676, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 760676, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 128.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158240, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283448, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 432264, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 880596, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 880596, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158240, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:14')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 283264, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 7, 0, 431976, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 20, 0, 1381056, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1381056, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 158752, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.059765625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 282780, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 383444, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 757412, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 757412, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 2.059765625GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159136, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281948, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384336, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 778316, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 778316, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159200, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.52852419354839GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282644, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384868, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 743556, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 743556, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 2.52852419354839GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159264, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.57077822580646GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282816, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384004, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 738832, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 738832, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 2.57077822580646GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159272, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59190524193549GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 282632, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 383544, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 744640, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 744640, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 2.59190524193549GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159272, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60246875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:11')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282148, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 384944, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 740812, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 740812, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 2.60246875GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159272, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60775050403226GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282940, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384952, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 770992, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 770992, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 2.60775050403226GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159272, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282944, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 430588, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 899912, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 899912, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159272, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282460, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384508, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 13, 0, 746152, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 746152, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159280, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282756, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 431136, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 919308, 'I(4, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 919308, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159280, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282076, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 386676, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 760792, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 760792, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159280, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282876, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384048, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 743136, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 743136, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159284, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 437.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282336, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 384912, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 774616, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 774616, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 437.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159284, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 283096, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 432852, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 20, 0, 1419092, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1419092, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159284, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 281628, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 4, 0, 384436, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 745528, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 745528, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159284, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.28125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282312, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 431688, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 20, 0, 1365088, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 12.6837, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1365088, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 1.28125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159288, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.1375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:10')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 282532, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false)', true, true)
					ProfileItem('Matrix Assembly', 2, 0, 5, 0, 385088, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 114421, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 4, 0, 14, 0, 745268, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 148756, false, 3, \'Matrix bandwidth\', 8.1142, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 745268, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 1.1375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 159288, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'143 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:11:11\', 1, \'Average memory/process\', \'1.35 GB\', 1, \'Max memory/process\', \'1.35 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 114421, false, 2, \'Max matrix size\', 148756, false, 1, \'Matrix bandwidth\', \'12.7\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:58:55\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:59:00')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:00:05')
			I(1, 'ComEngine Memory', '155 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 1 task, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 144 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:59:02')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:03')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 14:59:02')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:03')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 88.75MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 77.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 43.75MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 32.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 21.25MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 15.625MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 3.25MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.125MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 1.5625MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 1.28125MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 1.55GHz; S Matrix Error = 136.675%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.275GHz; S Matrix Error =  47.000%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 1.9125GHz; S Matrix Error =   5.992%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.6375GHz; S Matrix Error =   5.944%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.09375GHz; S Matrix Error =   4.525%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.81875GHz; S Matrix Error =   0.350%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.003125GHz; S Matrix Error =   0.433%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.184375GHz; S Matrix Error =   0.686%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 2.909375GHz; S Matrix Error =   0.575%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 550MHz; S Matrix Error =   0.452%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 662.5MHz; S Matrix Error =   0.046%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 775MHz; S Matrix Error =   0.074%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 1GHz; S Matrix Error =   0.030%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 1.1375GHz; S Matrix Error =   0.035%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 2.0484375GHz; S Matrix Error =   0.096%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 2.059765625GHz; S Matrix Error =   0.192%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.07109375GHz; S Matrix Error =   0.149%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 2.1390625GHz; S Matrix Error =   0.012%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.44401612903226GHz; Scattering matrix quantities converged; Passivity Error =   0.003649; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 2.52852419354839GHz; Scattering matrix quantities converged; Passivity Error =   0.086955; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 2.72102325581395GHz; Scattering matrix quantities converged; Passivity Error =   0.006792; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 128.125MHz; Scattering matrix quantities converged; Passivity Error =   0.006887; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 156.25MHz; Scattering matrix quantities converged; Passivity Error =   0.007129; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 212.5MHz; Scattering matrix quantities converged; Passivity Error =   0.007304; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 325MHz; Scattering matrix quantities converged; Passivity Error =   0.007479; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 437.5MHz; Scattering matrix quantities converged; Passivity Error =   0.007648; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 1.275GHz; Scattering matrix quantities converged; Passivity Error =   0.008229; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 2.082421875GHz; Scattering matrix quantities converged; Passivity Error =   0.026874; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 2.0880859375GHz; Scattering matrix quantities converged; Passivity Error =   0.008816; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 2.57077822580646GHz; Scattering matrix quantities converged; Passivity Error =   0.095176; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 2.59190524193549GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 2.60246875GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 2.60775050403226GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 2.61303225806452GHz; Scattering matrix quantities converged; Passive within tolerance\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using previously solved data. No additional simulations required\')', false, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'144 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:03\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 14:59:06\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 16:22:27')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:00:08')
			I(1, 'ComEngine Memory', '154 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 2 tasks, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 143 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 16:22:29')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:05')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep, Solving Distributed - up to 2 frequencies in parallel')
					I(1, 'Time', '08/09/2022 16:22:29')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:05')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 88.75MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 77.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 43.75MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 32.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 21.25MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 15.625MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 3.25MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.125MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 1.5625MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 1.28125MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 1.55GHz; S Matrix Error = 136.675%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.275GHz; S Matrix Error =  47.000%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 1.9125GHz; S Matrix Error =   5.992%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.6375GHz; S Matrix Error =   5.944%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.09375GHz; S Matrix Error =   4.525%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 2.81875GHz; S Matrix Error =   0.350%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.003125GHz; S Matrix Error =   0.433%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.184375GHz; S Matrix Error =   0.686%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 2.909375GHz; S Matrix Error =   0.575%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 550MHz; S Matrix Error =   0.452%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 662.5MHz; S Matrix Error =   0.046%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 775MHz; S Matrix Error =   0.074%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 1GHz; S Matrix Error =   0.030%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 1.1375GHz; S Matrix Error =   0.035%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 2.0484375GHz; S Matrix Error =   0.096%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 2.059765625GHz; S Matrix Error =   0.192%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.07109375GHz; S Matrix Error =   0.149%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 2.1390625GHz; S Matrix Error =   0.012%\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.44401612903226GHz; Scattering matrix quantities converged; Passivity Error =   0.003649; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 2.52852419354839GHz; Scattering matrix quantities converged; Passivity Error =   0.086955; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 2.72102325581395GHz; Scattering matrix quantities converged; Passivity Error =   0.006792; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 128.125MHz; Scattering matrix quantities converged; Passivity Error =   0.006887; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 156.25MHz; Scattering matrix quantities converged; Passivity Error =   0.007129; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 212.5MHz; Scattering matrix quantities converged; Passivity Error =   0.007304; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 325MHz; Scattering matrix quantities converged; Passivity Error =   0.007479; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 437.5MHz; Scattering matrix quantities converged; Passivity Error =   0.007648; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 1.275GHz; Scattering matrix quantities converged; Passivity Error =   0.008229; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 2.082421875GHz; Scattering matrix quantities converged; Passivity Error =   0.026874; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 2.0880859375GHz; Scattering matrix quantities converged; Passivity Error =   0.008816; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 2.57077822580646GHz; Scattering matrix quantities converged; Passivity Error =   0.095176; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 2.59190524193549GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 2.60246875GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 2.60775050403226GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 2.61303225806452GHz; Scattering matrix quantities converged; Passive within tolerance\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using previously solved data. No additional simulations required\')', false, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Distributed Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'143 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:05\', 1, \'Total Memory\', \'0 Bytes\')', false, true)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 16:22:35\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
