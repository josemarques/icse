$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2022
		MinorVer=2
		Name='Solution Process'
		$begin 'StartInfo'
			I(1, 'Start Time', '08/09/2022 14:59:08')
			I(1, 'Host', 'mrqs-srv-0')
			I(1, 'Processor', '24')
			I(1, 'OS', 'Linux 5.15.0-43-generic')
			I(1, 'Product', 'HFSS Version 2022.2.0')
		$end 'StartInfo'
		$begin 'TotalInfo'
			I(1, 'Elapsed Time', '00:14:47')
			I(1, 'ComEngine Memory', '168 M')
		$end 'TotalInfo'
		GroupOptions=8
		TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /opt/AnsysEM/v222/Linux64/HFSSCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Allow off core\', \'True\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC settings\', \'Manual\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Distribution Types\', \'Variations, Frequencies, Domain Solver, Transient Excitations, Mesh Assembly\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Two level\', \'Disabled\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Machines:\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'mrqs-srv-0 [62.8 GB]: RAM Limit: 90.000000%, 1 task, 4 cores, Free Disk Space: 247 GB\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'Solution Basis Order\', \'0\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , HFSS ComEngine Memory : 167 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations with standard port validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Mesh Refinement', 0, 0, 0, 0, 0, 'I(1, 0, \'Lambda Based\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Initial Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:59:08')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:00:15')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Mesh (lambda based)', 8, 0, 8, 0, 131520, 'I(1, 2, \'Tetrahedra\', 75108, false)', true, true)
			ProfileItem('Simulation Setup', 2, 0, 2, 0, 177872, 'I(2, 1, \'Disk\', \'0 Bytes\', 0, \'\')', true, true)
			ProfileItem('Port Adaptation', 0, 0, 0, 0, 193420, 'I(2, 1, \'Disk\', \'4.96 KB\', 2, \'Tetrahedra\', 49780, false)', true, true)
			ProfileItem('Mesh (port based)', 3, 0, 3, 0, 119288, 'I(1, 2, \'Tetrahedra\', 75331, false)', true, true)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Adaptive Meshing'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 14:59:24')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:01:31')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 1'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 14:59:24')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:07')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 1, 0, 1, 0, 185536, 'I(2, 1, \'Disk\', \'30.4 KB\', 2, \'Tetrahedra\', 49970, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 2, 0, 229560, 'I(3, 1, \'Disk\', \'64.4 KB\', 2, \'Tetrahedra\', 49970, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 5, 0, 494620, 'I(3, 1, \'Disk\', \'1.49 KB\', 2, \'Matrix size\', 71722, false, 3, \'Matrix bandwidth\', 7.62011, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 494620, 'I(2, 1, \'Disk\', \'6.78 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171364, 'I(1, 0, \'Adaptive Pass 1\')', true, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 2'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 14:59:31')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:14')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 5, 0, 5, 0, 140588, 'I(1, 2, \'Tetrahedra\', 90325, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 208496, 'I(2, 1, \'Disk\', \'36.7 KB\', 2, \'Tetrahedra\', 64223, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 3, 0, 265820, 'I(3, 1, \'Disk\', \'860 Bytes\', 2, \'Tetrahedra\', 64223, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 1, 0, 7, 0, 603848, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 88759, false, 3, \'Matrix bandwidth\', 7.80456, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 0, 0, 3, 0, 603848, 'I(2, 1, \'Disk\', \'3.3 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171364, 'I(1, 0, \'Adaptive Pass 2\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.188452, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 3'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 14:59:46')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:18')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 6, 0, 6, 0, 155084, 'I(1, 2, \'Tetrahedra\', 109594, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 2, 0, 2, 0, 251900, 'I(2, 1, \'Disk\', \'43.1 KB\', 2, \'Tetrahedra\', 82833, false)', true, true)
				ProfileItem('Matrix Assembly', 1, 0, 3, 0, 309400, 'I(3, 1, \'Disk\', \'280 Bytes\', 2, \'Tetrahedra\', 82833, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 2, 0, 9, 0, 804404, 'I(3, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 110967, false, 3, \'Matrix bandwidth\', 7.96064, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 4, 0, 804404, 'I(2, 1, \'Disk\', \'4.06 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171484, 'I(1, 0, \'Adaptive Pass 3\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0455626, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 4'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:00:04')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:21')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 8, 0, 8, 0, 172788, 'I(1, 2, \'Tetrahedra\', 134446, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 3, 0, 3, 0, 286416, 'I(2, 1, \'Disk\', \'52.2 KB\', 2, \'Tetrahedra\', 106890, false)', true, true)
				ProfileItem('Matrix Assembly', 2, 0, 4, 0, 366980, 'I(3, 1, \'Disk\', \'135 Bytes\', 2, \'Tetrahedra\', 106890, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 4, 0, 13, 0, 1087240, 'I(3, 1, \'Disk\', \'5 Bytes\', 2, \'Matrix size\', 139640, false, 3, \'Matrix bandwidth\', 8.08862, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 5, 0, 1087240, 'I(2, 1, \'Disk\', \'5.04 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171484, 'I(1, 0, \'Adaptive Pass 4\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.0218643, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Adaptive Pass 5'
				$begin 'StartInfo'
					I(0, 'Frequency:  1GHz')
					I(1, 'Time', '08/09/2022 15:00:26')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:00:28')
				$end 'TotalInfo'
				GroupOptions=0
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('Mesh (volume, adaptive)', 9, 0, 9, 0, 195300, 'I(1, 2, \'Tetrahedra\', 160130, false)', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('Simulation Setup ', 4, 0, 4, 0, 325272, 'I(2, 1, \'Disk\', \'63.7 KB\', 2, \'Tetrahedra\', 131635, false)', true, true)
				ProfileItem('Matrix Assembly', 3, 0, 6, 0, 427508, 'I(3, 1, \'Disk\', \'1.28 KB\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
				ProfileItem('Solver DCS4', 6, 0, 20, 0, 1351772, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\')', true, true)
				ProfileItem('Field Recovery', 1, 0, 6, 0, 1351772, 'I(2, 1, \'Disk\', \'5.54 MB\', 2, \'Excitations\', 2, false)', true, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171500, 'I(1, 0, \'Adaptive Pass 5\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 3, \'Max Mag. Delta S\', 0.00844878, \'%.5f\')', false, true)
			$end 'ProfileGroup'
			ProfileFootnote('I(1, 0, \'Adaptive Passes converged\')', 0)
		$end 'ProfileGroup'
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Frequency Sweep'
			$begin 'StartInfo'
				I(1, 'Time', '08/09/2022 15:00:55')
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(1, 'Elapsed Time', '00:12:59')
			$end 'TotalInfo'
			GroupOptions=4
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 1, \'HPC\', \'Enabled\')', false, true)
			$begin 'ProfileGroup'
				MajorVer=2022
				MinorVer=2
				Name='Solution - Sweep'
				$begin 'StartInfo'
					I(0, 'Interpolating HFSS Frequency Sweep')
					I(1, 'Time', '08/09/2022 15:00:55')
				$end 'StartInfo'
				$begin 'TotalInfo'
					I(1, 'Elapsed Time', '00:12:59')
				$end 'TotalInfo'
				GroupOptions=4
				TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'From 1MHz to 3GHz, 4000 Frequencies\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Using automatic algorithm to locate minimum frequency for the sweep.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 100MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #1; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311640, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426596, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 819772, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 819772, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 55MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #2; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309880, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 427236, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 881632, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 881632, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 10MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #3; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311088, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 479544, 'I(3, 1, \'Disk\', \'18 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 22, 0, 967884, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 967884, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 5.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #4; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309676, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 8, 0, 480056, 'I(3, 1, \'Disk\', \'12 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 23, 0, 981700, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 981700, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:17')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #5; Automatic determination of minimum frequency\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311020, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 480076, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 8, 0, 26, 0, 1581920, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1581920, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #6; Required Frequency\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311852, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 425480, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 884308, 'I(4, 1, \'Disk\', \'7 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 884308, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 1, Frequency: 3GHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 2, Frequency: 1MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 3, Frequency: 100MHz; New subrange(s) added; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 4, Frequency: 55MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 5, Frequency: 10MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 6, Frequency: 5.5MHz; Additional basis points are needed before the interpolation error can be computed.\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.55GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #7\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311632, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426664, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 906480, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 906480, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 7, Frequency: 1.55GHz; S Matrix Error = 135.265%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171912, 'I(1, 0, \'Frequency #7;  Interpolating frequency sweep\')', true, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Frequency: 1GHz has already been solved\')', false, true)
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 8, Frequency: 1GHz; S Matrix Error =  62.158%\')', false, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #8\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311572, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426864, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 847008, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 847008, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 9, Frequency: 2.275GHz; S Matrix Error =   2.923%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171976, 'I(1, 0, \'Frequency #8;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.6375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #9\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309872, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 425656, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 855268, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 855268, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 10, Frequency: 2.6375GHz; S Matrix Error =   2.044%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171976, 'I(1, 0, \'Frequency #9;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.81875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #10\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311184, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426028, 'I(3, 1, \'Disk\', \'9 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 831376, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 831376, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 11, Frequency: 2.81875GHz; S Matrix Error =   1.166%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171976, 'I(1, 0, \'Frequency #10;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 550MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #11\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309816, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 428660, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 869612, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 869612, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 12, Frequency: 550MHz; S Matrix Error =   0.924%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171976, 'I(1, 0, \'Frequency #11;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.9125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #12\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311180, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426348, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 828408, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 828408, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 13, Frequency: 1.9125GHz; S Matrix Error =   0.751%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171976, 'I(1, 0, \'Frequency #12;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.09375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #13\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 310212, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426872, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 855576, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 855576, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 14, Frequency: 2.09375GHz; S Matrix Error =   0.597%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171976, 'I(1, 0, \'Frequency #13;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.003125GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #14\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 310444, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 427060, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 843140, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 843140, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 15, Frequency: 2.003125GHz; S Matrix Error =   0.069%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171980, 'I(1, 0, \'Frequency #14;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0484375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #15\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309900, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 7, 0, 426300, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 926572, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 926572, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 16, Frequency: 2.0484375GHz; S Matrix Error =   0.105%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171980, 'I(1, 0, \'Frequency #15;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.07109375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #16\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 310768, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 427572, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 879660, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 879660, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 17, Frequency: 2.07109375GHz; S Matrix Error =   0.351%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171984, 'I(1, 0, \'Frequency #16;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.082421875GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #17\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 309944, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426284, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 844792, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 844792, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 18, Frequency: 2.082421875GHz; S Matrix Error =   0.441%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171984, 'I(1, 0, \'Frequency #17;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.0880859375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #18\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 310072, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426640, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 905780, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 905780, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 19, Frequency: 2.0880859375GHz; S Matrix Error =   0.118%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171984, 'I(1, 0, \'Frequency #18;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.184375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #19\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 310056, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426028, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 901796, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 901796, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 20, Frequency: 2.184375GHz; S Matrix Error =   0.018%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171984, 'I(1, 0, \'Frequency #19;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.1390625GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #20\')', false, true)
					ProfileItem('Simulation Setup ', 5, 0, 5, 0, 311160, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426088, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 841104, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 841104, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 21, Frequency: 2.1390625GHz; Scattering matrix quantities converged; Passivity Error =   0.000477; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171984, 'I(1, 0, \'Frequency #20;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 216.981995498875MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #21\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311764, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426760, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 865068, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 865068, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 22, Frequency: 216.981995498875MHz; Scattering matrix quantities converged; Passivity Error =   0.000323; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171984, 'I(1, 0, \'Frequency #21;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.71802350587647GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #22\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309900, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 427264, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 905604, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 905604, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 23, Frequency: 2.71802350587647GHz; Scattering matrix quantities converged; Passivity Error =   0.000509; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171988, 'I(1, 0, \'Frequency #22;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.61153238309577GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #23\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311344, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426268, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 771616, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 771616, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 24, Frequency: 2.61153238309577GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171992, 'I(1, 0, \'Frequency #23;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 32.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #24\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311764, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 478808, 'I(3, 1, \'Disk\', \'12 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 22, 0, 1000648, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1000648, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 25, Frequency: 32.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171996, 'I(1, 0, \'Frequency #24;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 77.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #25\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311792, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 427068, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 842112, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 842112, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 26, Frequency: 77.5MHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171996, 'I(1, 0, \'Frequency #25;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.44326619154789GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #26\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311556, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426740, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 818720, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 818720, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 27, Frequency: 2.44326619154789GHz; S Matrix Error =   0.001%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171996, 'I(1, 0, \'Frequency #26;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 88.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #27\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311740, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 427576, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 885344, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 885344, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 28, Frequency: 88.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171996, 'I(1, 0, \'Frequency #27;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 383.490997749437MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #28\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 310584, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 425772, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 846420, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 846420, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 29, Frequency: 383.490997749437MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171996, 'I(1, 0, \'Frequency #28;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 300.236496624156MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #29\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311644, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426448, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 902404, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 902404, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 30, Frequency: 300.236496624156MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171996, 'I(1, 0, \'Frequency #29;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 158.490997749437MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #30\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311696, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 427068, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 826608, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 826608, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 31, Frequency: 158.490997749437MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 171996, 'I(1, 0, \'Frequency #30;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 3.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #31\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311704, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 8, 0, 482492, 'I(3, 1, \'Disk\', \'28 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 22, 0, 908568, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 908568, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 32, Frequency: 3.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172000, 'I(1, 0, \'Frequency #31;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.125MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #32\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311892, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 479684, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 26, 0, 1610704, 'I(3, 1, \'Disk\', \'1 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1610704, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 33, Frequency: 2.125MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172000, 'I(1, 0, \'Frequency #32;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.909375GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #33\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 310020, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426792, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 869192, 'I(4, 1, \'Disk\', \'5 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 869192, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 34, Frequency: 2.909375GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172000, 'I(1, 0, \'Frequency #33;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.52739928732183GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #34\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309604, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426944, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 868572, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 868572, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 35, Frequency: 2.52739928732183GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172000, 'I(1, 0, \'Frequency #34;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60928257064266GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #35\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 310828, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 425624, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 911108, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 911108, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 36, Frequency: 2.60928257064266GHz; Scattering matrix quantities converged; Passive within tolerance; Adding basis elements to reach the desired minimum number of solutions\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172004, 'I(1, 0, \'Frequency #35;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.56834092898224GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #36\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311824, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 424784, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 844488, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 844488, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 37, Frequency: 2.56834092898224GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172004, 'I(1, 0, \'Frequency #36;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 21.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:15')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #37\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311916, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 8, 0, 481244, 'I(3, 1, \'Disk\', \'14 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 24, 0, 1001216, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1001216, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 38, Frequency: 21.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172008, 'I(1, 0, \'Frequency #37;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.58881174981245GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #38\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311532, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426356, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 865688, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 865688, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 39, Frequency: 2.58881174981245GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172008, 'I(1, 0, \'Frequency #38;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 15.625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #39\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311096, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 479732, 'I(3, 1, \'Disk\', \'13 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 6, 0, 22, 0, 956852, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 956852, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 40, Frequency: 15.625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172008, 'I(1, 0, \'Frequency #39;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.59904716022755GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #40\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311604, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426968, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 851288, 'I(4, 1, \'Disk\', \'4 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 851288, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 41, Frequency: 2.59904716022755GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172008, 'I(1, 0, \'Frequency #40;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 775MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #41\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309800, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426168, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 815968, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 815968, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 42, Frequency: 775MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172012, 'I(1, 0, \'Frequency #41;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 662.5MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #42\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311660, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426288, 'I(3, 1, \'Disk\', \'10 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 889072, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 889072, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 43, Frequency: 662.5MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172016, 'I(1, 0, \'Frequency #42;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 43.75MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #43\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311888, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 428164, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 859300, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 859300, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 44, Frequency: 43.75MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172016, 'I(1, 0, \'Frequency #43;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 129.245498874719MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #44\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 309872, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 425972, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 843368, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 843368, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 45, Frequency: 129.245498874719MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172016, 'I(1, 0, \'Frequency #44;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 66.25MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #45\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311120, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426496, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 870064, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 870064, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 46, Frequency: 66.25MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172016, 'I(1, 0, \'Frequency #45;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 114.622749437359MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #46\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 310296, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 425444, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 827312, 'I(4, 1, \'Disk\', \'2 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 827312, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 47, Frequency: 114.622749437359MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172148, 'I(1, 0, \'Frequency #46;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.5625MHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:16')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #47\')', false, true)
					ProfileItem('Simulation Setup ', 3, 0, 3, 0, 311720, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 4, 0, 7, 0, 479260, 'I(3, 1, \'Disk\', \'27 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 7, 0, 26, 0, 1570564, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 12.7754, \'%5.1f\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 1570564, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 48, Frequency: 1.5625MHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172152, 'I(1, 0, \'Frequency #47;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 1.275GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:12')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #48\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311724, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 5, 0, 426556, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 18, 0, 884784, 'I(4, 1, \'Disk\', \'7 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 884784, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 49, Frequency: 1.275GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172152, 'I(1, 0, \'Frequency #48;  Interpolating frequency sweep\')', true, true)
				ProfileItem(' ', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
				$begin 'ProfileGroup'
					MajorVer=2022
					MinorVer=2
					Name='Frequency - 2.60416486543511GHz'
					$begin 'StartInfo'
						I(0, 'mrqs-srv-0')
					$end 'StartInfo'
					$begin 'TotalInfo'
						I(0, 'Elapsed time : 00:00:13')
					$end 'TotalInfo'
					GroupOptions=0
					TaskDataOptions('CPU Time'=8, 'Real Time'=8)
					ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'HFSS: Single Frequency Solve Group #49\')', false, true)
					ProfileItem('Simulation Setup ', 4, 0, 4, 0, 311932, 'I(2, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false)', true, true)
					ProfileItem('Matrix Assembly', 3, 0, 6, 0, 426476, 'I(3, 1, \'Disk\', \'0 Bytes\', 2, \'Tetrahedra\', 131635, false, 2, \'Lumped ports\', 2, false)', true, true)
					ProfileItem('Solver DCS4', 5, 0, 17, 0, 868696, 'I(4, 1, \'Disk\', \'0 Bytes\', 2, \'Matrix size\', 169196, false, 3, \'Matrix bandwidth\', 8.17221, \'%5.1f\', 0, \'s-matrix only solve\')', true, true)
					ProfileItem('Field Recovery', 0, 0, 0, 0, 868696, 'I(2, 1, \'Disk\', \'4.67 KB\', 2, \'Excitations\', 2, false)', true, true)
				$end 'ProfileGroup'
				ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Basis Element # 50, Frequency: 2.60416486543511GHz; S Matrix Error =   0.000%\')', false, true)
				ProfileItem('Data Transfer', 0, 0, 0, 0, 172152, 'I(1, 0, \'Frequency #49;  Interpolating frequency sweep\')', true, true)
				ProfileFootnote('I(1, 0, \'Interpolating sweep converged and is passive\')', 0)
				ProfileFootnote('I(1, 0, \'HFSS: Interpolating sweep\')', 0)
			$end 'ProfileGroup'
		$end 'ProfileGroup'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		$begin 'ProfileGroup'
			MajorVer=2022
			MinorVer=2
			Name='Simulation Summary'
			$begin 'StartInfo'
			$end 'StartInfo'
			$begin 'TotalInfo'
				I(0, ' ')
			$end 'TotalInfo'
			GroupOptions=0
			TaskDataOptions('CPU Time'=8, Memory=8, 'Real Time'=8)
			ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:00\', 1, \'Total Memory\', \'167 MB\')', false, true)
			ProfileItem('Initial Meshing', 0, 0, 0, 0, 0, 'I(2, 1, \'Elapsed Time\', \'00:00:15\', 1, \'Total Memory\', \'189 MB\')', false, true)
			ProfileItem('Adaptive Meshing', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:01:31\', 1, \'Average memory/process\', \'1.29 GB\', 1, \'Max memory/process\', \'1.29 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileItem('Frequency Sweep', 0, 0, 0, 0, 0, 'I(5, 1, \'Elapsed Time\', \'00:12:59\', 1, \'Average memory/process\', \'1.54 GB\', 1, \'Max memory/process\', \'1.54 GB\', 2, \'Total number of processes\', 1, false, 2, \'Total number of cores\', 4, false)', false, true)
			ProfileFootnote('I(3, 2, \'Max solved tets\', 131635, false, 2, \'Max matrix size\', 169196, false, 1, \'Matrix bandwidth\', \'8.2\')', 0)
		$end 'ProfileGroup'
		ProfileFootnote('I(2, 1, \'Stop Time\', \'08/09/2022 15:13:55\', 1, \'Status\', \'Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
