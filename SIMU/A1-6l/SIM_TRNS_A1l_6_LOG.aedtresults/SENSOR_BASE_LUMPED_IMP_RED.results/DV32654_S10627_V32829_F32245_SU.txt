    VersionID HFSS2022
    NumberFrequencies 1
    Frequency 1550000000.000000
    Success 1
    SolverName Nondomain
    IterativeSolver 0
    DistributedSolver 0
    Precision 1
    NumTasks 1
    NumTets 116929
    BasisOrder 0
    TREECOTREE 0
    MemoryMasterMB 326.000000
    MemoryTotalMB 867.000000
    NumMeshStatisticsTets 116929
    MemoryForSMatrixOnlyMB 601.050781
    LowFrequencyCutoff 2670658.526453
    MemoryDistributedTotalMB -1.000000
    MemoryDistributedSMatrixOnlyMB -1.000000
    MemoryTotalAccurate 0
    MemorySMatrixOnlyAccurate 1
    MemoryDistributedTotalAccurate 0
    MemoryDistributedSMatrixOnlyAccurate 0
    SingularMatrix 0
    NumCores 2
    NumKrylovVectors -1
    MatrixSize 151907
    TotalModes 2
    NnzGrowthFactor 21.646617
