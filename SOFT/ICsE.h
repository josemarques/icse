#ifndef ICsE_H_INCLUDED
#define ICsE_H_INCLUDED

//Voltage controlled astable tuned oscillator

#include <algorithm>    // std::sort
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <math.h>
#include <string>
#include <functional>
#include <limits>


class ICsE{//Inducto-capacitive sensing element
	public:

	std::string REF="A1l_6";//ICsE Reference

	//State variables
	double F=0;//Main frequency // \ln(\SRF)=m_\mathrm{NPs} \: A_{1}-A_{0}
	double W=0;//Main power
	double H=0;//Main phase

	std::vector<double> Fs;//Frequency spectrum
	std::vector<double> Ws;//Power spectrum
	std::vector<double> Hs;//Phase spectrum
	//State variables


    //Analitic variables
    double L0=26.29*1e-9;
    double mu_eff=1;//Effective magnetic permeability
    double C0=0.386*1e-12;
    double epsilon_eff=6.07;//Effective electric permittivity
    double R0=0.349;

    double Z_L0();
    double Z_C0();
    double Z_R0();
    double Z_EQ();

    void SRF();//Compute self Resonant frequency
	//Analitic variables


    //RR Radio refractometry variables
	double n_env_eff;
	double n_smp_eff;
	double f;//Sample volumetric fraction

	double A0;//A_{0}=\ln(2\pi \sqrt{L\:C} \: n'_\mathrm{env})
	void CA0();//Compute A0

	double A1;//A_{1}=-\:\frac{f}{m_\mathrm{max}}\ln\left(\frac{n'_\mathrm{NPs}}{n'_\mathrm{env}}
	void CA1();//Compute A1

	double m;//Sample mass
	void CM();//Compute mass from F, A0, and A1
	//RR Radio refractometry variables

	ICsE();
	ICsE(const ICsE& ICsE);//Duplicador de clase
	ICsE& operator=(const ICsE& icse);//Igualador de clase

};

class ICTsE{//Inducto capacitive tuneable sensing element
public:

	ICTsE();
	ICTsE(const ICTsE& ictsE);//Duplicador de clase
	ICTsE& operator=(const ICTsE& ictse);//Igualador de clase

	std::string REF="A1l-6;SMV1231";//ICsE Reference; ICsE;VARACTRMODEL
	//State variables
	double F=0;//Main frequency // \ln(\SRF)=m_\mathrm{NPs} \: A_{1}-A_{0}
	double W=0;//Main power
	double H=0;//Main phase

	std::vector<double> Fs;//Frequency spectrum
	std::vector<double> Ws;//Power spectrum
	std::vector<double> Hs;//Phase spectrum
	//State variables

	//Analitic variables
    double L0=26.29*1e-9;
    double C0=0.386*1e-12;
    double R0=0.349;

	double VV=0;//Varactor voltage
	double c0=2.34*1e-12,c1=-3.295*1e-12,c2=0.945*1e-12;//Varactor pair parameter
	double C_V;//Varactor capacitance C_V=c0+c1*VV+c2*VV*VV;//

	double epsilon_eff=6.07;//Effective electric permittivity
	double mu_eff=1;//Effective magnetic permeability
	double rf_m_eff=0;//Effective relational magnetic funciontal variable
	double rf_e_eff=0;//Effective relational electrical funciontal variable

	double Z_L0();
	double Z_C0();
	double Z_VC();
	double Z_C();
	double Z_R0();
	double Z_EQ();

    void SRF();//Compute self Resonant frequency

};

#endif
