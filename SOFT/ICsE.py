 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#Inducto capacitive sensing element Analitic, numerical and transfer functions calculation

import sys, getopt

import control
import matplotlib.pyplot as plt

import sympy
import math

import TFAN


def VCC_x(v_c,o_0,o_1,o_2):#voltage control capacitance expresion
	C_vc=o_0+o_1*v_c+o_2*v_c**2

	return C_vc

def VCC_aC():#voltage control capacitance analytic calculation
	V_c=sympy.Symbol("V_c",real=True)
	o_0=sympy.Symbol("o_0",real=True)
	o_1=sympy.Symbol("o_1",real=True)
	o_2=sympy.Symbol("o_2",real=True)

	C_vc=VCC_x(V_c,o_0,o_1,o_2)

	return C_vc

def ZICsE_x(R_S,C_S,L_S,s):#Impedance of the Inducto capacitive sensing element expression

	Z_RS=R_S
	Z_CS=1/(C_S*s)
	Z_LS=L_S*s
	Z_S=1/((1/(Z_RS+Z_LS))+(1/(Z_CS)))
	#print("Z_S=",Z_S)

	return Z_S

def ZICsE_aC():#Impedance of the Inducto capacitive sensing element analytic calculation

	s=sympy.Symbol("s")
	R_S=sympy.Symbol("R_S",real=True)
	C_S0=sympy.Symbol("C_S0",real=True)
	epr=sympy.Symbol("epr",real=True)
	L_S0=sympy.Symbol("L_S0",real=True)
	mur=sympy.Symbol("mur",real=True)

	Z_S=ZICsE_x(R_S,C_S0*epr,L_S0*mur,s)
	#print("Z_S=",Z_S)

	return Z_S

def ZICVCsE_aC():#Impedance of the Inducto capacitive voltage controled sensing element analytic calculation

	s=sympy.Symbol("s")
	R_S=sympy.Symbol("R_S",real=True)
	C_S0=sympy.Symbol("C_S0",real=True)
	epr=sympy.Symbol("epr",real=True)
	L_S0=sympy.Symbol("L_S0",real=True)
	mur=sympy.Symbol("mur",real=True)
	R_o=sympy.Symbol("R_o",real=True)


	Z_S=ZICsE_x(R_S,C_S0*epr+VCC_aC(),L_S0*mur,s)
	#print("Z_S=",Z_S)

	return Z_S

def ICsE_tFaC():#Inducto capacitive sensing element transfer function analytic calculation
	R_o=sympy.Symbol("R_o",real=True)

	V_IN=sympy.Symbol("V_IN")

	Z_S=ZICsE_aC()

	Z_Ro=R_o

	Z_T=1/((1/(Z_Ro))+(1/(Z_Ro+Z_S)))
	I=V_IN/Z_T
	I1=V_IN/Z_Ro
	I2=I-I1
	V_OUT=I2*Z_Ro
	EIC=sympy.factor(V_OUT/V_IN)
	EICn,EICd = sympy.fraction(EIC)

	#print("EICn",EICn)
	#print("EICd",EICd)
	#print("EIC=",EIC)

	return EIC

def ICVCsE_tFaC():#Inducto capacitive voltage control sensing element transfer function analytic calculation
	R_o=sympy.Symbol("R_o",real=True)

	V_IN=sympy.Symbol("V_IN")

	Z_S=ZICVCsE_aC()

	Z_Ro=R_o

	Z_T=1/((1/(Z_Ro))+(1(Z_Ro+Z_S)))
	I=V_IN/Z_T
	I1=V_IN/Z_Ro
	I2=I-I1
	V_OUT=I2*Z_Ro
	EIC=sympy.factor(V_OUT/V_IN)
	EICn,EICd = sympy.fraction(EIC)

	#print("EICn",EICn)
	#print("EICd",EICd)
	#print("EIC=",EIC)

	return EIC



def ICsE_tFC(c_s,l_s,r_s,r_o): #Inducto-capacitive transfer function calculation

	ICTfn=[c_s*l_s*r_o,c_s*r_s*r_o,r_o]
	ICTfd=[c_s*l_s*r_o,l_s+c_s*r_s*r_o,r_s+r_o]
	ICTf=control.tf(ICTfn,ICTfd)
	#print("ICTf(s)=",ICTf)
	return ICTf

def ZICsE_tFC(c_s,l_s,r_s):#impedance transfer function calculation
	ZICsEn=[l_s,r_s]
	ZICsEd=[c_s*l_s,c_s*r_s,1]
	ZICsE=control.tf(ZICsEn,ZICsEd)
	#print("ZICsE(s)=",ZICsE)
	return (ZICsE)

def SRF_ICsE_aC():#Self resonant frequency inducto capacitive sensing element analytic calculation
	Z_Sa=ZICVCsE_aC()
	#print("\nZ_Sa=",Z_Sa,"\n")
	omega=sympy.Symbol("omega",real=True)
	Z_Sa=Z_Sa.subs(sympy.Symbol("s"),(omega*sympy.I))
	#Z_Sa=sympy.simplify(Z_Sa)
	#print("\nZ_Sa=",Z_Sa,"\n")

	#Compute the imaginary part of the impedance in phasor
	Z_Sa_i=sympy.im(Z_Sa)
	Z_Sa_i=sympy.simplify(Z_Sa_i)
	#print("\nZ_Sa_i=",Z_Sa_i,"\n")

	#Compute the SRF as the omega at which imaginary part of the impedance becomes 0
	srfs=sympy.solve(Z_Sa_i,omega)
	#print("\nsrfs=",srfs,"\n")
	srf=srfs[1]
	#print("\nsrf=",sympy.simplify(srf),"\n")

	return srf

def SRF_ICVCsE_pCd_aC():#self reseonant frequency of inducto capacitive voltage controlled sensing element parameter calibration data analitic calculation
	srf=SRF_ICsE_aC()

	srf=srf.subs(sympy.Symbol("R_S",real=True),0)#Asume R_S=0
	#print("\nsrf=",srf,"\n")
	fsrf=srf**(-2)#Modification of SRF to simplify parameters
	fsrf=sympy.simplify(fsrf)
	fsrf=sympy.expand(fsrf)

	#print("\nfsrf=",fsrf, "\n")

	#For calibration we assume mur=1, epr=1
	#fsrf=fsrf.subs(sympy.Symbol("mur",real=True),1)#Asume R_S=0
	#fsrf=fsrf.subs(sympy.Symbol("epr",real=True),1)#Asume R_S=0

	#print("\nfsrf=",fsrf, "\n")

	#Calibration variables
	V_c=sympy.Symbol("V_c",real=True)
	fsrfp=sympy.Poly(fsrf,V_c)
	y_0,y_1,y_2=fsrfp.coeffs()

	y=[y_2,y_1,y_0]

	print("\ny_2=",y_2)
	print("y_1=",y_1)
	print("y_0=",y_0,"\n")

	fsrf=fsrf.subs(y_0,sympy.Symbol("y_0",real=True))# Change parameters in expression
	fsrf=fsrf.subs(y_1,sympy.Symbol("y_1",real=True))# Change parameters in expression
	fsrf=fsrf.subs(y_2,sympy.Symbol("y_2",real=True))# Change parameters in expression

	print("fsrf=",fsrf)

	return fsrf, y

def SRF_ICVCsE_Cd_aC():#self reseonant frequency of inducto capacitive voltage controlled sensing element calibration data analitic calculation

	frsf,y=SRF_ICVCsE_pCd_aC()

	y_m=list(y)
	y_c=list(y)

	for i in range(len(y_c)):
		y_c[i]=y_c[i].subs(sympy.Symbol("mur",real=True),1)
		y_c[i]=y_c[i].subs(sympy.Symbol("epr",real=True),1)

	print("\ny_m=",y_m,"\n")
	print("\ny_c=",y_c,"\n")


	mu=((y_m[2]/y_c[2])+(y_m[1]/y_c[1]))/2
	mu=sympy.simplify(mu)
	print("\nmu=",mu,"\n")

	print("\ny_m[0]=",sympy.simplify(y_m[0]),"\n")
	print("\ny_c[0]=",sympy.simplify(y_c[0]),"\n")


	Y0M_Y0C=sympy.Symbol("y0m_y0c",real=True)
	epsr=sympy.solve((y_m[0]/mur)-y_c[0]-Y0M_Y0C,epr)
	print("\nepsr=",epsr,"\n")

	return 0


def SRF_ICVCsE_bCd_aC():#impedance of inducto capacitive voltage cotnrolled sensing element behavioural calibration data analitic calculation

	srf=SRF_ICsE_aC()

	srf=srf.subs(sympy.Symbol("R_S",real=True),0)#Asume R_S=0

	fsrf=srf**(-2)#Modification of SRF to simplify parameters
	#print("\nfsrf=",fsrf, "\n")

	dfsrf=sympy.diff(fsrf, sympy.Symbol("V_c",real=True))


	return (fsrf, dfsrf)

def SRF_ICVCsE_bCd_saPC():#impedance of inducto capacitive voltage cotnrolled sensing element behavioural calibration data susceptibility procedure calculation

	fsrf_c, dfsrf_c=SRF_ICVCsE_bCd_aC()#Calibration data
	fsrf_c=fsrf_c.subs(sympy.Symbol("mur",real=True),1)# Change parameters in expression
	fsrf_c=fsrf_c.subs(sympy.Symbol("epr",real=True),1)# Change parameters in expression
	dfsrf_c=dfsrf_c.subs(sympy.Symbol("mur",real=True),1)# Change parameters in expression
	dfsrf_c=dfsrf_c.subs(sympy.Symbol("epr",real=True),1)# Change parameters in expression
	#print("\nfsrf_c=",fsrf_c, "\n")
	#print("\ndfsrf_c=",dfsrf_c, "\n")

	fsrf_m, dfsrf_m=SRF_ICVCsE_bCd_aC()#Measurement data
	fsrf_m=fsrf_m.subs(sympy.Symbol("mur",real=True),sympy.Symbol("mur",real=True)*sympy.Symbol("delt",real=True))# Change parameters in expression
	fsrf_m=fsrf_m.subs(sympy.Symbol("epr",real=True),sympy.Symbol("epr",real=True)*sympy.Symbol("delt",real=True))# Change parameters in expression
	dfsrf_m=dfsrf_m.subs(sympy.Symbol("mur",real=True),sympy.Symbol("mur",real=True)*sympy.Symbol("delt",real=True))# Change parameters in expression
	dfsrf_m=dfsrf_m.subs(sympy.Symbol("epr",real=True),sympy.Symbol("epr",real=True)*sympy.Symbol("delt",real=True))# Change parameters in expression
	#print("\nfsrf_m=",fsrf_m, "\n")
	#print("\ndfsrf_m=",dfsrf_m, "\n")

	mure=dfsrf_m/dfsrf_c
	mure=sympy.simplify(mure)
	#print("\nmure=",mure, "\n")
	mse=mure.subs(sympy.Symbol("mur",real=True),sympy.Symbol("ms",real=True)+1)
	mse=mse.subs(sympy.Symbol("delt",real=True),sympy.Symbol("ds",real=True)+1)
	#print("\nmse=",mse, "\n")
	mse=sympy.expand(mse)
	mse=mse.subs(sympy.Symbol("ds",real=True)*sympy.Symbol("ms",real=True),0)
	mse=mse.subs(sympy.Symbol("ds",real=True)+1,sympy.Symbol("delt",real=True))
	print("\nmse=",mse, "\n")

	#print("")

	epre=(fsrf_m/mure)-(fsrf_c)
	#epr=sympy(fsrf_m/mure)/(fsrf_c)

	epre=sympy.simplify(epre)
	#print("\nepre=",epre, "\n")
	ese=epre.subs(sympy.Symbol("epr",real=True),sympy.Symbol("es",real=True)+1)
	ese=ese.subs(sympy.Symbol("delt",real=True),sympy.Symbol("ds",real=True)+1)

	ese=ese.subs(sympy.Symbol("C_S0",real=True)*sympy.Symbol("L_S0",real=True),1)
	ese=sympy.expand(ese)
	ese=ese*sympy.Symbol("C_S0",real=True)*sympy.Symbol("L_S0",real=True)
	#print("\nese=",ese, "\n")

	ese=ese.subs(sympy.Symbol("ds",real=True)*sympy.Symbol("es",real=True),0)
	ese=ese.subs(sympy.Symbol("ds",real=True)+1,sympy.Symbol("delt",real=True))

	print("\nese=",ese, "\n")

	#Reference material calibration
	XMref=sympy.Symbol("XMref",real=True)
	XMrefF=mse#Magnetic reference measurement
	msref=sympy.Symbol("msref",real=True)#Real Magnetic susceptibility of the measurement
	XMrefF=XMrefF.subs(sympy.Symbol("ms",real=True),msref)
	print("\nXMrefF=",XMrefF,"\n")
	delt=sympy.solve(XMrefF-XMref,sympy.Symbol("delt",real=True))[0]
	msF=mse.subs(sympy.Symbol("delt",real=True),delt)
	print("\nmsF=",msF,"\n") #Magnetic susceptibility function
	mS=sympy.solve(msF-sympy.Symbol("XmS",real=True),sympy.Symbol("ms",real=True))[0]#Magnetic susceptibility with calibration and reference materila
	print("\nmS=",mS,"\n") #Magnetic susceptibility function

	XEref=sympy.Symbol("XEref",real=True)#Electric susceptibility reference measurment
	XErefF=ese#Electric susceptibility reference measurment function
	esref=sympy.Symbol("esref",real=True)#Real electric susceptibility of the measurement
	XErefF=XErefF.subs(sympy.Symbol("es",real=True),msref)
	print("\nXErefF=",XErefF,"\n")
	LS0CS0=sympy.solve(XErefF-XEref,sympy.Symbol("L_S0",real=True)*sympy.Symbol("C_S0",real=True))[0]
	print("\nLS0CS0=",LS0CS0,"\n")
	esF=ese.subs(sympy.Symbol("L_S0",real=True)*sympy.Symbol("C_S0",real=True),LS0CS0)
	esF=esF.subs(sympy.Symbol("delt",real=True),delt)
	print("\nesF=",esF,"\n") #Magnetic susceptibility function
	eS=sympy.solve(esF-sympy.Symbol("XeS",real=True),sympy.Symbol("es",real=True))[0]#Electric susceptibility with calibration and reference materila
	print("\neS=",eS,"\n") #Magnetic susceptibility function

def SRF_ICVCsE_bCd_ppaPC():#impedance of inducto capacitive voltage cotnrolled sensing element behavioural calibration data permeability permittivity procedure calculation

	fsrf_c, dfsrf_c=SRF_ICVCsE_bCd_aC()#Calibration data
	# fsrf_c=fsrf_c.subs(sympy.Symbol("mur",real=True),1)# Change parameters in expression
	# fsrf_c=fsrf_c.subs(sympy.Symbol("epr",real=True),1)# Change parameters in expression
	# dfsrf_c=dfsrf_c.subs(sympy.Symbol("mur",real=True),1)# Change parameters in expression
	# dfsrf_c=dfsrf_c.subs(sympy.Symbol("epr",real=True),1)# Change parameters in expression
	fsrf_c=fsrf_c.subs(sympy.Symbol("mur",real=True),sympy.Symbol("murC",real=True))# Change parameters in expression
	fsrf_c=fsrf_c.subs(sympy.Symbol("epr",real=True),sympy.Symbol("eprC",real=True))# Change parameters in expression
	dfsrf_c=dfsrf_c.subs(sympy.Symbol("mur",real=True),sympy.Symbol("murC",real=True))# Change parameters in expression
	dfsrf_c=dfsrf_c.subs(sympy.Symbol("epr",real=True),sympy.Symbol("eprC",real=True))# Change parameters in expression

	print("\nfsrf_c=",fsrf_c, "\n")
	print("\ndfsrf_c=",dfsrf_c, "\n")

	fsrf_m, dfsrf_m=SRF_ICVCsE_bCd_aC()#Measurement data
	fsrf_m=fsrf_m.subs(sympy.Symbol("mur",real=True),sympy.Symbol("mur",real=True)*sympy.Symbol("delt",real=True))# Change parameters in expression
	fsrf_m=fsrf_m.subs(sympy.Symbol("epr",real=True),sympy.Symbol("epr",real=True)*sympy.Symbol("delt",real=True))# Change parameters in expression
	dfsrf_m=dfsrf_m.subs(sympy.Symbol("mur",real=True),sympy.Symbol("mur",real=True)*sympy.Symbol("delt",real=True))# Change parameters in expression
	dfsrf_m=dfsrf_m.subs(sympy.Symbol("epr",real=True),sympy.Symbol("epr",real=True)*sympy.Symbol("delt",real=True))# Change parameters in expression
	print("\nfsrf_m=",fsrf_m, "\n")
	print("\ndfsrf_m=",dfsrf_m, "\n")

	mure=dfsrf_m/dfsrf_c#Magnetic permeability function
	mure=sympy.simplify(mure)
	print("\nmure=",mure, "\n")

	epre=(fsrf_m/mure)-(fsrf_c)#Electric permittivity function
	epre=sympy.simplify(epre)
	print("\nepre=",epre, "\n")


	#Reference material calibration
	MUref=sympy.Symbol("MUref",real=True)#Magnetic permeability reference function value
	MUrefF=mure#Magnetic permeability reference function
	muref=sympy.Symbol("muref",real=True)#Reference magnetic permeability
	MUrefF=MUrefF.subs(sympy.Symbol("mur",real=True),muref)
	print("\nMUrefF=",MUrefF,"\n")
	delt=sympy.solve(MUrefF-MUref,sympy.Symbol("delt",real=True))[0]
	print("\ndelt=",delt,"\n")
	MU=sympy.Symbol("MU",real=True)#Magnetic susceptibility function value
	MUF=mure.subs(sympy.Symbol("delt",real=True),delt)#Magnetic susceptibility function
	print("\nMUF=",MUF,"\n")
	mu=sympy.solve(MUF-MU,sympy.Symbol("mur",real=True))[0]#Magnetic susceptibility with calibration and reference materila
	print("\nmu=",mu,"\n") #Magnetic susceptibility function

	EPref=sympy.Symbol("EPref",real=True)#Electric susceptibility reference function value
	EPrefF=epre#Electric susceptibility reference measurment function
	epref=sympy.Symbol("epref",real=True)#Reference electric permittivity
	EPrefF=EPrefF.subs(sympy.Symbol("epr",real=True),epref)
	print("\nEPrefF=",EPrefF,"\n")
	LS0CS0=sympy.solve(EPrefF-EPref,sympy.Symbol("L_S0",real=True)*sympy.Symbol("C_S0",real=True))[0]
	print("\nLS0CS0=",LS0CS0,"\n")
	EPF=epre.subs(sympy.Symbol("L_S0",real=True)*sympy.Symbol("C_S0",real=True),LS0CS0)
	EPF=EPF.subs(sympy.Symbol("delt",real=True),delt)
	print("\nEPF=",EPF,"\n") #Magnetic susceptibility function
	EP=sympy.Symbol("EP",real=True)#Electric permittivity function value
	ep=sympy.solve(EPF-EP,sympy.Symbol("epr",real=True))[0]#Electric susceptibility with calibration and reference materila
	print("\nep=",ep,"\n") #Magnetic susceptibility function

def SRF_ICVCsE_bCd_nPC(srfs_c, srfs_m, v_vcs):#impedance of inducto capacitive voltage cotnrolled sensing element behavioural calibration data numerical procedure calculation

	assert (len(srfs_c)==2), "needed 2 srf_c values"
	assert (len(srfs_m)==2), "needed 2 srf_c values"
	assert (len(v_vcs)==2), "needed 2 srf_c values"

	fsrfs_c=[]
	fsrfs_m=[]

	for i in range(2):
		fsrfs_c.append(srfs_c[i]**(-2))
		fsrfs_m.append(srfs_m[i]**(-2))

	if(v_vcs[0]<v_vcs[1]):
		dfsrfs_c=(fsrfs_c[1]-fsrfs_c[0])/(v_vcs[1]-v_vcs[0])
		dfsrfs_m=(fsrfs_m[1]-fsrfs_m[0])/(v_vcs[1]-v_vcs[0])

	elif(v_vcs[0]>v_vcs[1]):
		dfsrfs_c=(fsrfs_c[0]-fsrfs_c[1])/(v_vcs[0]-v_vcs[1])
		dfsrfs_m=(fsrfs_m[0]-fsrfs_m[1])/(v_vcs[0]-v_vcs[1])
	else:
		raise Exception("v_vcs need two diferent values")

	fsrfs_c=(fsrfs_c[0]+fsrfs_c[1])/2
	fsrfs_m=(fsrfs_m[0]+fsrfs_m[1])/2

	sm=((dfsrfs_m/dfsrfs_c)-1)#magnetic susceptibility
	print("\nsm=",sm,"\n")

	lcse=(fsrfs_m/mur)-fsrfs_c#L_S0*C_S0*(epr-1)#Variable proportional to electric susceptibility
	print("\nlcse=",lcse,"\n")

	return(sm,lcse)

#System variables
R_S=1
L_S0=36*10**-9
mur=1
L_S=L_S0*mur
epr=1
C_S0=1.5*10**-12
C_SV=1.5*10**-12
C_S=C_S0*epr+C_SV
R_o=1

TSTP=1*10**(-7)


def main(argv):
	#Functon Calculations
	SRF_ICVCsE_pCd_aC()


	print("VCC_aC()=",VCC_aC())

	ICsE=ICsE_tFC(C_S,L_S,R_S,R_o)

	z = control.zero(ICsE)
	p = control.pole(ICsE)
	print("Zeroes ")
	print(z)
	print("Poles ")
	print(p)

	ZICsE=ZICsE_tFC(C_S,L_S,R_S)

	#control.pzmap(CS)

	# control.bode_plot(ICsE)
	# control.bode_plot(ZICsE)
	# plt.grid()
	# plt.show()

	#Calibration
	print("CALIBRATION")

	#SRF_ICVCsE_bCd_saPC()

	SRF_ICVCsE_bCd_ppaPC()


	# sm_p,lcse_p=SRF_ICVCsE_bCd_nPC([(450*10**6),(450*10**6)+500000], [(450*10**6)-20540,(450*10**6)+500000-19810], [2.5,2.4])
 #
	# sm_m,lcse_m=SRF_ICVCsE_bCd_nPC([(450*10**6),(450*10**6)+400000], [(450*10**6)-20540,(450*10**6)+400000-30810], [2.5,2.4])
 #
	# sm_mp=sm_m/sm_p
	# lcse_mp=lcse_m/lcse_p
 #
	# print("\nsm_mp=",sm_mp,"\n")
	# print("\nlsse_mp=",lcse_mp,"\n")




if __name__ == "__main__":
	main(sys.argv[1:])
