#include "ICsE.h"

ICsE::ICsE(){//Constructor
}

ICsE::ICsE(const ICsE& icse){

}

ICsE& ICsE::operator=(const ICsE& icse){

	return *this;
} //Igualador de clase

void ICsE::CA0(){//Compute A0
	A0=log(2*M_PI*sqrt(L0*C0)*n_env_eff);
}

void ICsE::CA1(){//Compute A0
	A1=-(f)*log(n_smp_eff/n_env_eff);
}

void ICsE::CM(){//Compute A0
	m=(log(F)+A0)/A1;
}


ICTsE::ICTsE(){//Constructor
}

ICTsE::ICTsE(const ICTsE& ictse){
	REF=ictse.REF;//ICsE Reference; ICsE;VARACTRMODEL

	//State variables
	F=ictse.F;//Main frequency // \ln(\SRF)=m_\mathrm{NPs} \: A_{1}-A_{0}
	W=ictse.W;//Main power
	H=ictse.H;//Main phase

	Fs=ictse.Fs;//Frequency spectrum
	Ws=ictse.Ws;//Power spectrum
	Hs=ictse.Hs;//Phase spectrum
	//State variables

	//Analitic variables
	L0=ictse.L0;//Sensor base self-inductance
	C0=ictse.C0;//Sensor base self-capacitance
	R0=ictse.R0;//Sensor resistance

	VV=ictse.VV;//Varactor voltage
	c0=ictse.c0;
	c1=ictse.c1;
	c2=ictse.c2;//Varactor pair parameter
	C_V=ictse.C_V;//Varactor capacitance C_V=c0+c1*VV+c2*VV*VV;//

	epsilon_eff=ictse.epsilon_eff;//Effective electric permittivity
	mu_eff=ictse.mu_eff;//Effective magnetic permeability
	rf_m_eff=ictse.rf_m_eff;
	rf_e_eff=ictse.rf_m_eff;
}

ICTsE& ICTsE::operator=(const ICTsE& ictse){

	REF=ictse.REF;//ICsE Reference; ICsE;VARACTRMODEL

	//State variables
	F=ictse.F;//Main frequency // \ln(\SRF)=m_\mathrm{NPs} \: A_{1}-A_{0}
	W=ictse.W;//Main power
	H=ictse.H;//Main phase

	Fs=ictse.Fs;//Frequency spectrum
	Ws=ictse.Ws;//Power spectrum
	Hs=ictse.Hs;//Phase spectrum
	//State variables

	//Analitic variables
	L0=ictse.L0;//Sensor base self-inductance
	C0=ictse.C0;//Sensor base self-capacitance
	R0=ictse.R0;//Sensor resistance

	VV=ictse.VV;//Varactor voltage
	c0=ictse.c0;
	c1=ictse.c1;
	c2=ictse.c2;//Varactor pair parameter
	C_V=ictse.C_V;//Varactor capacitance C_V=c0+c1*VV+c2*VV*VV;//

	epsilon_eff=ictse.epsilon_eff;//Effective electric permittivity
	mu_eff=ictse.mu_eff;//Effective magnetic permeability
	rf_m_eff=ictse.rf_m_eff;
	rf_e_eff=ictse.rf_m_eff;

	return *this;
} //Igualador de clase
