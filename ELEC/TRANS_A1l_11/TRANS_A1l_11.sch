EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5EAFBCA5
P 5100 1000
F 0 "J1" V 5064 812 50  0000 R CNN
F 1 "Conn_01x02" V 4973 812 50  0000 R CNN
F 2 "PPMS_ANALOG_RF:SCRW_CON_2X1" H 5100 1000 50  0001 C CNN
F 3 "~" H 5100 1000 50  0001 C CNN
	1    5100 1000
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5EAFC205
P 6750 1000
F 0 "J2" V 6714 912 50  0000 R CNN
F 1 "Conn_01x01" V 6623 912 50  0000 R CNN
F 2 "PPMS_ANALOG_RF:RF_SREW_HOLE_2.5mm" H 6750 1000 50  0001 C CNN
F 3 "~" H 6750 1000 50  0001 C CNN
	1    6750 1000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5100 1200 5100 1600
Wire Wire Line
	5100 1600 5200 1600
Wire Wire Line
	5200 1600 5200 1200
Wire Wire Line
	6750 1200 6750 1450
Wire Wire Line
	6750 1450 7400 1450
Wire Wire Line
	7400 1450 7400 1200
Wire Wire Line
	8100 1200 8100 1450
Wire Wire Line
	8100 1450 7750 1450
Connection ~ 7400 1450
Wire Wire Line
	8750 1200 8750 1450
Wire Wire Line
	8750 1450 8100 1450
Connection ~ 8100 1450
$Comp
L power:GND #PWR0101
U 1 1 5EAFE9E3
P 7750 1700
F 0 "#PWR0101" H 7750 1450 50  0001 C CNN
F 1 "GND" H 7755 1527 50  0000 C CNN
F 2 "" H 7750 1700 50  0001 C CNN
F 3 "" H 7750 1700 50  0001 C CNN
	1    7750 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1700 7750 1450
Connection ~ 7750 1450
Wire Wire Line
	7750 1450 7400 1450
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 5EB0085E
P 7400 1000
F 0 "J3" V 7364 912 50  0000 R CNN
F 1 "Conn_01x01" V 7273 912 50  0000 R CNN
F 2 "PPMS_ANALOG_RF:RF_SREW_HOLE_2.5mm" H 7400 1000 50  0001 C CNN
F 3 "~" H 7400 1000 50  0001 C CNN
	1    7400 1000
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 5EB00C5F
P 8100 1000
F 0 "J4" V 8064 912 50  0000 R CNN
F 1 "Conn_01x01" V 7973 912 50  0000 R CNN
F 2 "PPMS_ANALOG_RF:RF_SREW_HOLE_2.5mm" H 8100 1000 50  0001 C CNN
F 3 "~" H 8100 1000 50  0001 C CNN
	1    8100 1000
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 5EB012AA
P 8750 1000
F 0 "J5" V 8714 912 50  0000 R CNN
F 1 "Conn_01x01" V 8623 912 50  0000 R CNN
F 2 "PPMS_ANALOG_RF:RF_SREW_HOLE_2.5mm" H 8750 1000 50  0001 C CNN
F 3 "~" H 8750 1000 50  0001 C CNN
	1    8750 1000
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
